package io.insurance.discovery;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 *
 * @author mehenna
 * @version 0.0.1-SNAPSHOT
 */

@RunWith(SpringRunner.class)
@SpringBootTest
public class DiscoveryServiceApplicationTests {

    @Test
    public void contextLoads() {
    }

}

