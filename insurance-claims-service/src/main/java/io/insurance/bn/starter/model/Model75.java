/*
 * API documentation
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: 1.0.0-20181108155220-20181108155343
 * Contact: api.move@axa-assistance.com
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */


package io.insurance.bn.starter.model;

import com.google.gson.annotations.SerializedName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.util.Objects;

/**
 * Agency where the car was rent.
 */
@ApiModel(description = "Agency where the car was rent.")
@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaClientCodegen", date = "2018-12-19T23:23:06.995+01:00")
public class Model75 {
  @SerializedName("name")
  private String name = null;

  @SerializedName("address")
  private Model73 address = null;

  @SerializedName("phone")
  private Model74 phone = null;

  public Model75 name(String name) {
    this.name = name;
    return this;
  }

   /**
   * description: Name of the agency.
   * @return name
  **/
  @ApiModelProperty(value = "description: Name of the agency.")
  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public Model75 address(Model73 address) {
    this.address = address;
    return this;
  }

   /**
   * Get address
   * @return address
  **/
  @ApiModelProperty(value = "")
  public Model73 getAddress() {
    return address;
  }

  public void setAddress(Model73 address) {
    this.address = address;
  }

  public Model75 phone(Model74 phone) {
    this.phone = phone;
    return this;
  }

   /**
   * Get phone
   * @return phone
  **/
  @ApiModelProperty(value = "")
  public Model74 getPhone() {
    return phone;
  }

  public void setPhone(Model74 phone) {
    this.phone = phone;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Model75 model75 = (Model75) o;
    return Objects.equals(this.name, model75.name) &&
        Objects.equals(this.address, model75.address) &&
        Objects.equals(this.phone, model75.phone);
  }

  @Override
  public int hashCode() {
    return Objects.hash(name, address, phone);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Model75 {\n");
    
    sb.append("    name: ").append(toIndentedString(name)).append("\n");
    sb.append("    address: ").append(toIndentedString(address)).append("\n");
    sb.append("    phone: ").append(toIndentedString(phone)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

