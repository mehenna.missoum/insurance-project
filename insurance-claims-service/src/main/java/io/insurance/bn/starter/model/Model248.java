/*
 * API documentation
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: 1.0.0-20181108155220-20181108155343
 * Contact: api.move@axa-assistance.com
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */


package io.insurance.bn.starter.model;

import com.google.gson.annotations.SerializedName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.util.Objects;

/**
 * Bank account linked to the reimbursement.
 */
@ApiModel(description = "Bank account linked to the reimbursement.")
@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaClientCodegen", date = "2018-12-19T23:23:06.995+01:00")
public class Model248 {
  @SerializedName("account_codes")
  private AccountCodes accountCodes = null;

  @SerializedName("is_current_account")
  private Boolean isCurrentAccount = null;

  @SerializedName("bank")
  private Model247 bank = null;

  public Model248 accountCodes(AccountCodes accountCodes) {
    this.accountCodes = accountCodes;
    return this;
  }

   /**
   * Get accountCodes
   * @return accountCodes
  **/
  @ApiModelProperty(value = "")
  public AccountCodes getAccountCodes() {
    return accountCodes;
  }

  public void setAccountCodes(AccountCodes accountCodes) {
    this.accountCodes = accountCodes;
  }

  public Model248 isCurrentAccount(Boolean isCurrentAccount) {
    this.isCurrentAccount = isCurrentAccount;
    return this;
  }

   /**
   * Is it a current account?
   * @return isCurrentAccount
  **/
  @ApiModelProperty(value = "Is it a current account?")
  public Boolean isIsCurrentAccount() {
    return isCurrentAccount;
  }

  public void setIsCurrentAccount(Boolean isCurrentAccount) {
    this.isCurrentAccount = isCurrentAccount;
  }

  public Model248 bank(Model247 bank) {
    this.bank = bank;
    return this;
  }

   /**
   * Get bank
   * @return bank
  **/
  @ApiModelProperty(value = "")
  public Model247 getBank() {
    return bank;
  }

  public void setBank(Model247 bank) {
    this.bank = bank;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Model248 model248 = (Model248) o;
    return Objects.equals(this.accountCodes, model248.accountCodes) &&
        Objects.equals(this.isCurrentAccount, model248.isCurrentAccount) &&
        Objects.equals(this.bank, model248.bank);
  }

  @Override
  public int hashCode() {
    return Objects.hash(accountCodes, isCurrentAccount, bank);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Model248 {\n");
    
    sb.append("    accountCodes: ").append(toIndentedString(accountCodes)).append("\n");
    sb.append("    isCurrentAccount: ").append(toIndentedString(isCurrentAccount)).append("\n");
    sb.append("    bank: ").append(toIndentedString(bank)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

