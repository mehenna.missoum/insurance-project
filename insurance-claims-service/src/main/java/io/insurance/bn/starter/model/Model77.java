/*
 * API documentation
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: 1.0.0-20181108155220-20181108155343
 * Contact: api.move@axa-assistance.com
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */


package io.insurance.bn.starter.model;

import com.google.gson.annotations.SerializedName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.threeten.bp.LocalDate;

import java.util.Objects;

/**
 * Information related to the car incident.
 */
@ApiModel(description = "Information related to the car incident.")
@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaClientCodegen", date = "2018-12-19T23:23:06.995+01:00")
public class Model77 {
  @SerializedName("date")
  private LocalDate date = null;

  @SerializedName("driver")
  private Driver driver = null;

  @SerializedName("location")
  private Model76 location = null;

  @SerializedName("description")
  private String description = null;

  public Model77 date(LocalDate date) {
    this.date = date;
    return this;
  }

   /**
   * Date and time of the accident. UTC datetime, RFC3339 format (YYYY-MM-DDTHH:mm:ssZ).
   * @return date
  **/
  @ApiModelProperty(required = true, value = "Date and time of the accident. UTC datetime, RFC3339 format (YYYY-MM-DDTHH:mm:ssZ).")
  public LocalDate getDate() {
    return date;
  }

  public void setDate(LocalDate date) {
    this.date = date;
  }

  public Model77 driver(Driver driver) {
    this.driver = driver;
    return this;
  }

   /**
   * Get driver
   * @return driver
  **/
  @ApiModelProperty(required = true, value = "")
  public Driver getDriver() {
    return driver;
  }

  public void setDriver(Driver driver) {
    this.driver = driver;
  }

  public Model77 location(Model76 location) {
    this.location = location;
    return this;
  }

   /**
   * Get location
   * @return location
  **/
  @ApiModelProperty(value = "")
  public Model76 getLocation() {
    return location;
  }

  public void setLocation(Model76 location) {
    this.location = location;
  }

  public Model77 description(String description) {
    this.description = description;
    return this;
  }

   /**
   * Description of the incident.
   * @return description
  **/
  @ApiModelProperty(value = "Description of the incident.")
  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Model77 model77 = (Model77) o;
    return Objects.equals(this.date, model77.date) &&
        Objects.equals(this.driver, model77.driver) &&
        Objects.equals(this.location, model77.location) &&
        Objects.equals(this.description, model77.description);
  }

  @Override
  public int hashCode() {
    return Objects.hash(date, driver, location, description);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Model77 {\n");
    
    sb.append("    date: ").append(toIndentedString(date)).append("\n");
    sb.append("    driver: ").append(toIndentedString(driver)).append("\n");
    sb.append("    location: ").append(toIndentedString(location)).append("\n");
    sb.append("    description: ").append(toIndentedString(description)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

