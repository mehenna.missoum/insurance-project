/*
 * API documentation
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: 1.0.0-20181108155220-20181108155343
 * Contact: api.move@axa-assistance.com
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */


package io.insurance.bn.starter.model;

import com.google.gson.annotations.SerializedName;
import io.swagger.annotations.ApiModelProperty;

import java.util.Objects;

/**
 * Loyalty
 */
@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaClientCodegen", date = "2018-12-19T23:23:06.995+01:00")
public class Loyalty {
  @SerializedName("program")
  private String program = null;

  @SerializedName("level")
  private String level = null;

  public Loyalty program(String program) {
    this.program = program;
    return this;
  }

   /**
   * Loyalty program to which the customer is affiliated (e.g.: Flying Blue, ...)
   * @return program
  **/
  @ApiModelProperty(example = "Flying Blue", required = true, value = "Loyalty program to which the customer is affiliated (e.g.: Flying Blue, ...)")
  public String getProgram() {
    return program;
  }

  public void setProgram(String program) {
    this.program = program;
  }

  public Loyalty level(String level) {
    this.level = level;
    return this;
  }

   /**
   * Level of the loyalty program to which the customer is affiliated (e.g.:Platinium, Gold,...)
   * @return level
  **/
  @ApiModelProperty(example = "Gold", required = true, value = "Level of the loyalty program to which the customer is affiliated (e.g.:Platinium, Gold,...)")
  public String getLevel() {
    return level;
  }

  public void setLevel(String level) {
    this.level = level;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Loyalty loyalty = (Loyalty) o;
    return Objects.equals(this.program, loyalty.program) &&
        Objects.equals(this.level, loyalty.level);
  }

  @Override
  public int hashCode() {
    return Objects.hash(program, level);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Loyalty {\n");
    
    sb.append("    program: ").append(toIndentedString(program)).append("\n");
    sb.append("    level: ").append(toIndentedString(level)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

