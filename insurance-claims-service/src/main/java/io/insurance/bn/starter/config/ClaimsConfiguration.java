package io.insurance.bn.starter.config;

import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import static springfox.documentation.builders.PathSelectors.any;


/**
 *
 * @author mehenna
 * @version 0.0.1
 *
 */


@Configuration
@EnableSwagger2
public class ClaimsConfiguration {

    @Bean
    @LoadBalanced
    public RestTemplate restTemplate() {
        return new RestTemplate();
    }


    @Bean
    public Docket api(){
        return new Docket(DocumentationType.SWAGGER_2).groupName("Insurance").select()
                .apis(RequestHandlerSelectors.basePackage("io.insurance.bn.starter"))
                .paths(any()).build().apiInfo(new ApiInfo("Insurance Services",
                        "A set of services to consum AXA end points", "1.0.0", null,
                        new Contact("Mehenna MISSOUM", "https://twitter.com/MEHENNAMISSOUM", "mehenna.missoum@tcs.com"),null, null));
    }
}
