/*
 * API documentation
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: 1.0.0-20181108155220-20181108155343
 * Contact: api.move@axa-assistance.com
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */


package io.insurance.bn.starter.model;

import com.google.gson.annotations.SerializedName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.util.Objects;

/**
 * Product criteria.
 */
@ApiModel(description = "Product criteria.")
@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaClientCodegen", date = "2018-12-19T23:23:06.995+01:00")
public class Model191 {
  @SerializedName("product_code")
  private String productCode = null;

  @SerializedName("catalog")
  private Model190 catalog = null;

  @SerializedName("category")
  private String category = null;

  @SerializedName("sub_category")
  private String subCategory = null;

  @SerializedName("is_cheapest")
  private Boolean isCheapest = false;

  public Model191 productCode(String productCode) {
    this.productCode = productCode;
    return this;
  }

   /**
   * Product code, used if the quote request is for a specific product.
   * @return productCode
  **/
  @ApiModelProperty(example = "PTRAVEL001", value = "Product code, used if the quote request is for a specific product.")
  public String getProductCode() {
    return productCode;
  }

  public void setProductCode(String productCode) {
    this.productCode = productCode;
  }

  public Model191 catalog(Model190 catalog) {
    this.catalog = catalog;
    return this;
  }

   /**
   * Get catalog
   * @return catalog
  **/
  @ApiModelProperty(required = true, value = "")
  public Model190 getCatalog() {
    return catalog;
  }

  public void setCatalog(Model190 catalog) {
    this.catalog = catalog;
  }

  public Model191 category(String category) {
    this.category = category;
    return this;
  }

   /**
   * Product category.
   * @return category
  **/
  @ApiModelProperty(example = "VoyageoCourtSejour", required = true, value = "Product category.")
  public String getCategory() {
    return category;
  }

  public void setCategory(String category) {
    this.category = category;
  }

  public Model191 subCategory(String subCategory) {
    this.subCategory = subCategory;
    return this;
  }

   /**
   * Product sub category.
   * @return subCategory
  **/
  @ApiModelProperty(example = "VoyageSimple", required = true, value = "Product sub category.")
  public String getSubCategory() {
    return subCategory;
  }

  public void setSubCategory(String subCategory) {
    this.subCategory = subCategory;
  }

  public Model191 isCheapest(Boolean isCheapest) {
    this.isCheapest = isCheapest;
    return this;
  }

   /**
   * Indicates if only the cheapest products has to be provided.
   * @return isCheapest
  **/
  @ApiModelProperty(example = "true", value = "Indicates if only the cheapest products has to be provided.")
  public Boolean isIsCheapest() {
    return isCheapest;
  }

  public void setIsCheapest(Boolean isCheapest) {
    this.isCheapest = isCheapest;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Model191 model191 = (Model191) o;
    return Objects.equals(this.productCode, model191.productCode) &&
        Objects.equals(this.catalog, model191.catalog) &&
        Objects.equals(this.category, model191.category) &&
        Objects.equals(this.subCategory, model191.subCategory) &&
        Objects.equals(this.isCheapest, model191.isCheapest);
  }

  @Override
  public int hashCode() {
    return Objects.hash(productCode, catalog, category, subCategory, isCheapest);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Model191 {\n");
    
    sb.append("    productCode: ").append(toIndentedString(productCode)).append("\n");
    sb.append("    catalog: ").append(toIndentedString(catalog)).append("\n");
    sb.append("    category: ").append(toIndentedString(category)).append("\n");
    sb.append("    subCategory: ").append(toIndentedString(subCategory)).append("\n");
    sb.append("    isCheapest: ").append(toIndentedString(isCheapest)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

