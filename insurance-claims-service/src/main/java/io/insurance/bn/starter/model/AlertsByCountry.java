/*
 * API documentation
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: 1.0.0-20181108155220-20181108155343
 * Contact: api.move@axa-assistance.com
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */


package io.insurance.bn.starter.model;

import com.google.gson.annotations.SerializedName;
import io.swagger.annotations.ApiModelProperty;

import java.util.Objects;

/**
 * AlertsByCountry
 */
@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaClientCodegen", date = "2018-12-19T23:23:06.995+01:00")
public class AlertsByCountry {
  @SerializedName("axa_health_alerts")
  private AxaHealthAlerts axaHealthAlerts = null;

  @SerializedName("axa_security_alerts")
  private AxaSecurityAlerts axaSecurityAlerts = null;

  public AlertsByCountry axaHealthAlerts(AxaHealthAlerts axaHealthAlerts) {
    this.axaHealthAlerts = axaHealthAlerts;
    return this;
  }

   /**
   * Get axaHealthAlerts
   * @return axaHealthAlerts
  **/
  @ApiModelProperty(required = true, value = "")
  public AxaHealthAlerts getAxaHealthAlerts() {
    return axaHealthAlerts;
  }

  public void setAxaHealthAlerts(AxaHealthAlerts axaHealthAlerts) {
    this.axaHealthAlerts = axaHealthAlerts;
  }

  public AlertsByCountry axaSecurityAlerts(AxaSecurityAlerts axaSecurityAlerts) {
    this.axaSecurityAlerts = axaSecurityAlerts;
    return this;
  }

   /**
   * Get axaSecurityAlerts
   * @return axaSecurityAlerts
  **/
  @ApiModelProperty(required = true, value = "")
  public AxaSecurityAlerts getAxaSecurityAlerts() {
    return axaSecurityAlerts;
  }

  public void setAxaSecurityAlerts(AxaSecurityAlerts axaSecurityAlerts) {
    this.axaSecurityAlerts = axaSecurityAlerts;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    AlertsByCountry alertsByCountry = (AlertsByCountry) o;
    return Objects.equals(this.axaHealthAlerts, alertsByCountry.axaHealthAlerts) &&
        Objects.equals(this.axaSecurityAlerts, alertsByCountry.axaSecurityAlerts);
  }

  @Override
  public int hashCode() {
    return Objects.hash(axaHealthAlerts, axaSecurityAlerts);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class AlertsByCountry {\n");
    
    sb.append("    axaHealthAlerts: ").append(toIndentedString(axaHealthAlerts)).append("\n");
    sb.append("    axaSecurityAlerts: ").append(toIndentedString(axaSecurityAlerts)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

