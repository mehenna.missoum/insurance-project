/*
 * API documentation
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: 1.0.0-20181108155220-20181108155343
 * Contact: api.move@axa-assistance.com
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */


package io.insurance.bn.starter.model;

import com.google.gson.annotations.SerializedName;
import io.swagger.annotations.ApiModelProperty;

import java.util.Objects;

/**
 * Model151
 */
@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaClientCodegen", date = "2018-12-19T23:23:06.995+01:00")
public class Model151 {
  @SerializedName("summary")
  private String summary = null;

  @SerializedName("link")
  private String link = null;

  @SerializedName("guarantees")
  private Model150 guarantees = null;

  public Model151 summary(String summary) {
    this.summary = summary;
    return this;
  }

   /**
   * Commercial text of the coverage
   * @return summary
  **/
  @ApiModelProperty(value = "Commercial text of the coverage")
  public String getSummary() {
    return summary;
  }

  public void setSummary(String summary) {
    this.summary = summary;
  }

  public Model151 link(String link) {
    this.link = link;
    return this;
  }

   /**
   * URL to the coverage details page
   * @return link
  **/
  @ApiModelProperty(value = "URL to the coverage details page")
  public String getLink() {
    return link;
  }

  public void setLink(String link) {
    this.link = link;
  }

  public Model151 guarantees(Model150 guarantees) {
    this.guarantees = guarantees;
    return this;
  }

   /**
   * Get guarantees
   * @return guarantees
  **/
  @ApiModelProperty(value = "")
  public Model150 getGuarantees() {
    return guarantees;
  }

  public void setGuarantees(Model150 guarantees) {
    this.guarantees = guarantees;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Model151 model151 = (Model151) o;
    return Objects.equals(this.summary, model151.summary) &&
        Objects.equals(this.link, model151.link) &&
        Objects.equals(this.guarantees, model151.guarantees);
  }

  @Override
  public int hashCode() {
    return Objects.hash(summary, link, guarantees);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Model151 {\n");
    
    sb.append("    summary: ").append(toIndentedString(summary)).append("\n");
    sb.append("    link: ").append(toIndentedString(link)).append("\n");
    sb.append("    guarantees: ").append(toIndentedString(guarantees)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

