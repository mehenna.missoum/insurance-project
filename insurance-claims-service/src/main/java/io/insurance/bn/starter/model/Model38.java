/*
 * API documentation
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: 1.0.0-20181108155220-20181108155343
 * Contact: api.move@axa-assistance.com
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */


package io.insurance.bn.starter.model;

import com.google.gson.annotations.SerializedName;
import io.swagger.annotations.ApiModelProperty;
import org.threeten.bp.LocalDate;

import java.util.Objects;

/**
 * Model38
 */
@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaClientCodegen", date = "2018-12-19T23:23:06.995+01:00")
public class Model38 {
  @SerializedName("name")
  private String name = null;

  @SerializedName("content_url")
  private String contentUrl = null;

  @SerializedName("type")
  private String type = null;

  @SerializedName("created_at")
  private LocalDate createdAt = null;

  public Model38 name(String name) {
    this.name = name;
    return this;
  }

   /**
   * Attached document name.
   * @return name
  **/
  @ApiModelProperty(required = true, value = "Attached document name.")
  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public Model38 contentUrl(String contentUrl) {
    this.contentUrl = contentUrl;
    return this;
  }

   /**
   * URL to download the document.
   * @return contentUrl
  **/
  @ApiModelProperty(required = true, value = "URL to download the document.")
  public String getContentUrl() {
    return contentUrl;
  }

  public void setContentUrl(String contentUrl) {
    this.contentUrl = contentUrl;
  }

  public Model38 type(String type) {
    this.type = type;
    return this;
  }

   /**
   * Indicates the type of the document, it can be: - TERMS_AND_CONDITIONS; - CONTRACT; - ...
   * @return type
  **/
  @ApiModelProperty(value = "Indicates the type of the document, it can be: - TERMS_AND_CONDITIONS; - CONTRACT; - ...")
  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }

  public Model38 createdAt(LocalDate createdAt) {
    this.createdAt = createdAt;
    return this;
  }

   /**
   * Date and time of document creation - UTC datetime, RFC3339 format (YYYY-MM-DDTHH:mm:ssZ).
   * @return createdAt
  **/
  @ApiModelProperty(value = "Date and time of document creation - UTC datetime, RFC3339 format (YYYY-MM-DDTHH:mm:ssZ).")
  public LocalDate getCreatedAt() {
    return createdAt;
  }

  public void setCreatedAt(LocalDate createdAt) {
    this.createdAt = createdAt;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Model38 model38 = (Model38) o;
    return Objects.equals(this.name, model38.name) &&
        Objects.equals(this.contentUrl, model38.contentUrl) &&
        Objects.equals(this.type, model38.type) &&
        Objects.equals(this.createdAt, model38.createdAt);
  }

  @Override
  public int hashCode() {
    return Objects.hash(name, contentUrl, type, createdAt);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Model38 {\n");
    
    sb.append("    name: ").append(toIndentedString(name)).append("\n");
    sb.append("    contentUrl: ").append(toIndentedString(contentUrl)).append("\n");
    sb.append("    type: ").append(toIndentedString(type)).append("\n");
    sb.append("    createdAt: ").append(toIndentedString(createdAt)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

