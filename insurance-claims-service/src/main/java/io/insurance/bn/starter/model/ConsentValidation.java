/*
 * API documentation
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: 1.0.0-20181108155220-20181108155343
 * Contact: api.move@axa-assistance.com
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */


package io.insurance.bn.starter.model;

import java.util.Objects;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.IOException;
import org.threeten.bp.LocalDate;

/**
 * ConsentValidation
 */
@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaClientCodegen", date = "2018-12-19T23:23:06.995+01:00")
public class ConsentValidation {
  @SerializedName("code")
  private String code = null;

  @SerializedName("is_confirmed")
  private Boolean isConfirmed = null;

  @SerializedName("consented_at")
  private LocalDate consentedAt = null;

  public ConsentValidation code(String code) {
    this.code = code;
    return this;
  }

   /**
   * Code of the consent such as - legal_consent_I_understand - legal_consent_I_authorize - ...
   * @return code
  **/
  @ApiModelProperty(required = true, value = "Code of the consent such as - legal_consent_I_understand - legal_consent_I_authorize - ...")
  public String getCode() {
    return code;
  }

  public void setCode(String code) {
    this.code = code;
  }

  public ConsentValidation isConfirmed(Boolean isConfirmed) {
    this.isConfirmed = isConfirmed;
    return this;
  }

   /**
   * description: Indicates if the consent has been confirmed or not.
   * @return isConfirmed
  **/
  @ApiModelProperty(required = true, value = "description: Indicates if the consent has been confirmed or not.")
  public Boolean isIsConfirmed() {
    return isConfirmed;
  }

  public void setIsConfirmed(Boolean isConfirmed) {
    this.isConfirmed = isConfirmed;
  }

  public ConsentValidation consentedAt(LocalDate consentedAt) {
    this.consentedAt = consentedAt;
    return this;
  }

   /**
   * Consent date. UTC datetime, RFC3339 format (YYYY-MM-DDTHH:mm:ssZ).
   * @return consentedAt
  **/
  @ApiModelProperty(value = "Consent date. UTC datetime, RFC3339 format (YYYY-MM-DDTHH:mm:ssZ).")
  public LocalDate getConsentedAt() {
    return consentedAt;
  }

  public void setConsentedAt(LocalDate consentedAt) {
    this.consentedAt = consentedAt;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    ConsentValidation consentValidation = (ConsentValidation) o;
    return Objects.equals(this.code, consentValidation.code) &&
        Objects.equals(this.isConfirmed, consentValidation.isConfirmed) &&
        Objects.equals(this.consentedAt, consentValidation.consentedAt);
  }

  @Override
  public int hashCode() {
    return Objects.hash(code, isConfirmed, consentedAt);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class ConsentValidation {\n");
    
    sb.append("    code: ").append(toIndentedString(code)).append("\n");
    sb.append("    isConfirmed: ").append(toIndentedString(isConfirmed)).append("\n");
    sb.append("    consentedAt: ").append(toIndentedString(consentedAt)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

