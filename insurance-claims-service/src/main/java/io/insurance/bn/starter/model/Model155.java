/*
 * API documentation
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: 1.0.0-20181108155220-20181108155343
 * Contact: api.move@axa-assistance.com
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */


package io.insurance.bn.starter.model;

import com.google.gson.annotations.SerializedName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.math.BigDecimal;
import java.util.Objects;

/**
 * Policy&#39;s price details.
 */
@ApiModel(description = "Policy's price details.")
@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaClientCodegen", date = "2018-12-19T23:23:06.995+01:00")
public class Model155 {
  @SerializedName("formula")
  private String formula = null;

  @SerializedName("currency")
  private String currency = null;

  @SerializedName("price_after_discount_inc_tax")
  private BigDecimal priceAfterDiscountIncTax = null;

  public Model155 formula(String formula) {
    this.formula = formula;
    return this;
  }

   /**
   * Formula selected by the customer.
   * @return formula
  **/
  @ApiModelProperty(example = "12-MONTHS", required = true, value = "Formula selected by the customer.")
  public String getFormula() {
    return formula;
  }

  public void setFormula(String formula) {
    this.formula = formula;
  }

  public Model155 currency(String currency) {
    this.currency = currency;
    return this;
  }

   /**
   * Currency, ISO 4217 format (3 letter code).
   * @return currency
  **/
  @ApiModelProperty(example = "EUR", required = true, value = "Currency, ISO 4217 format (3 letter code).")
  public String getCurrency() {
    return currency;
  }

  public void setCurrency(String currency) {
    this.currency = currency;
  }

  public Model155 priceAfterDiscountIncTax(BigDecimal priceAfterDiscountIncTax) {
    this.priceAfterDiscountIncTax = priceAfterDiscountIncTax;
    return this;
  }

   /**
   * Price amount discounted including taxes, numeric with 2 decimals.
   * @return priceAfterDiscountIncTax
  **/
  @ApiModelProperty(example = "150.25", required = true, value = "Price amount discounted including taxes, numeric with 2 decimals.")
  public BigDecimal getPriceAfterDiscountIncTax() {
    return priceAfterDiscountIncTax;
  }

  public void setPriceAfterDiscountIncTax(BigDecimal priceAfterDiscountIncTax) {
    this.priceAfterDiscountIncTax = priceAfterDiscountIncTax;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Model155 model155 = (Model155) o;
    return Objects.equals(this.formula, model155.formula) &&
        Objects.equals(this.currency, model155.currency) &&
        Objects.equals(this.priceAfterDiscountIncTax, model155.priceAfterDiscountIncTax);
  }

  @Override
  public int hashCode() {
    return Objects.hash(formula, currency, priceAfterDiscountIncTax);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Model155 {\n");
    
    sb.append("    formula: ").append(toIndentedString(formula)).append("\n");
    sb.append("    currency: ").append(toIndentedString(currency)).append("\n");
    sb.append("    priceAfterDiscountIncTax: ").append(toIndentedString(priceAfterDiscountIncTax)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

