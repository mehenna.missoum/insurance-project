/*
 * API documentation
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: 1.0.0-20181108155220-20181108155343
 * Contact: api.move@axa-assistance.com
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */


package io.insurance.bn.starter.model;

import java.util.Objects;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.IOException;

/**
 * Availability
 */
@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaClientCodegen", date = "2018-12-19T23:23:06.995+01:00")
public class Availability {
  @SerializedName("from")
  private String from = null;

  @SerializedName("to")
  private String to = null;

  public Availability from(String from) {
    this.from = from;
    return this;
  }

   /**
   * Beginning date and time of the availability - UTC datetime, RFC3339 format (YYYY-MM-DDTHH:mmZ)
   * @return from
  **/
  @ApiModelProperty(example = "1989-01-01T10:00:00Z", value = "Beginning date and time of the availability - UTC datetime, RFC3339 format (YYYY-MM-DDTHH:mmZ)")
  public String getFrom() {
    return from;
  }

  public void setFrom(String from) {
    this.from = from;
  }

  public Availability to(String to) {
    this.to = to;
    return this;
  }

   /**
   * End date and time of the availability - UTC datetime, RFC3339 format (YYYY-MM-DDTHH:mmZ)
   * @return to
  **/
  @ApiModelProperty(example = "1989-01-01T10:30:00Z", value = "End date and time of the availability - UTC datetime, RFC3339 format (YYYY-MM-DDTHH:mmZ)")
  public String getTo() {
    return to;
  }

  public void setTo(String to) {
    this.to = to;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Availability availability = (Availability) o;
    return Objects.equals(this.from, availability.from) &&
        Objects.equals(this.to, availability.to);
  }

  @Override
  public int hashCode() {
    return Objects.hash(from, to);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Availability {\n");
    
    sb.append("    from: ").append(toIndentedString(from)).append("\n");
    sb.append("    to: ").append(toIndentedString(to)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

