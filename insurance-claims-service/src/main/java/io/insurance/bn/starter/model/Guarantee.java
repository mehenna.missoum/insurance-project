/*
 * API documentation
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: 1.0.0-20181108155220-20181108155343
 * Contact: api.move@axa-assistance.com
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */


package io.insurance.bn.starter.model;

import com.google.gson.annotations.SerializedName;
import io.swagger.annotations.ApiModelProperty;

import java.util.Objects;

/**
 * Guarantee
 */
@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaClientCodegen", date = "2018-12-19T23:23:06.995+01:00")
public class Guarantee {
  @SerializedName("code")
  private String code = null;

  @SerializedName("label")
  private String label = null;

  @SerializedName("included")
  private Boolean included = null;

  @SerializedName("limit")
  private String limit = null;

  public Guarantee code(String code) {
    this.code = code;
    return this;
  }

   /**
   * Name of the top guarantee
   * @return code
  **/
  @ApiModelProperty(required = true, value = "Name of the top guarantee")
  public String getCode() {
    return code;
  }

  public void setCode(String code) {
    this.code = code;
  }

  public Guarantee label(String label) {
    this.label = label;
    return this;
  }

   /**
   * Description of the top guarantee
   * @return label
  **/
  @ApiModelProperty(value = "Description of the top guarantee")
  public String getLabel() {
    return label;
  }

  public void setLabel(String label) {
    this.label = label;
  }

  public Guarantee included(Boolean included) {
    this.included = included;
    return this;
  }

   /**
   * Defines if top guarantee is included
   * @return included
  **/
  @ApiModelProperty(value = "Defines if top guarantee is included")
  public Boolean isIncluded() {
    return included;
  }

  public void setIncluded(Boolean included) {
    this.included = included;
  }

  public Guarantee limit(String limit) {
    this.limit = limit;
    return this;
  }

   /**
   * Limit of top guarantee, when included
   * @return limit
  **/
  @ApiModelProperty(value = "Limit of top guarantee, when included")
  public String getLimit() {
    return limit;
  }

  public void setLimit(String limit) {
    this.limit = limit;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Guarantee guarantee = (Guarantee) o;
    return Objects.equals(this.code, guarantee.code) &&
        Objects.equals(this.label, guarantee.label) &&
        Objects.equals(this.included, guarantee.included) &&
        Objects.equals(this.limit, guarantee.limit);
  }

  @Override
  public int hashCode() {
    return Objects.hash(code, label, included, limit);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Guarantee {\n");
    
    sb.append("    code: ").append(toIndentedString(code)).append("\n");
    sb.append("    label: ").append(toIndentedString(label)).append("\n");
    sb.append("    included: ").append(toIndentedString(included)).append("\n");
    sb.append("    limit: ").append(toIndentedString(limit)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

