/*
 * API documentation
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: 1.0.0-20181108155220-20181108155343
 * Contact: api.move@axa-assistance.com
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */


package io.insurance.bn.starter.model;

import com.google.gson.annotations.SerializedName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.util.Objects;

/**
 * Person information
 */
@ApiModel(description = "Person information")
@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaClientCodegen", date = "2018-12-19T23:23:06.995+01:00")
public class Model27 {
  @SerializedName("title")
  private String title = null;

  @SerializedName("first_name")
  private String firstName = null;

  @SerializedName("last_name")
  private String lastName = null;

  @SerializedName("second_last_name")
  private String secondLastName = null;

  @SerializedName("email")
  private String email = null;

  @SerializedName("person_registrations")
  private Model26 personRegistrations = null;

  public Model27 title(String title) {
    this.title = title;
    return this;
  }

   /**
   * Civility. Expected values (MR, MS, MISS, DR, PR)
   * @return title
  **/
  @ApiModelProperty(value = "Civility. Expected values (MR, MS, MISS, DR, PR)")
  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public Model27 firstName(String firstName) {
    this.firstName = firstName;
    return this;
  }

   /**
   * First name
   * @return firstName
  **/
  @ApiModelProperty(required = true, value = "First name")
  public String getFirstName() {
    return firstName;
  }

  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }

  public Model27 lastName(String lastName) {
    this.lastName = lastName;
    return this;
  }

   /**
   * Last name
   * @return lastName
  **/
  @ApiModelProperty(required = true, value = "Last name")
  public String getLastName() {
    return lastName;
  }

  public void setLastName(String lastName) {
    this.lastName = lastName;
  }

  public Model27 secondLastName(String secondLastName) {
    this.secondLastName = secondLastName;
    return this;
  }

   /**
   * Second last name
   * @return secondLastName
  **/
  @ApiModelProperty(value = "Second last name")
  public String getSecondLastName() {
    return secondLastName;
  }

  public void setSecondLastName(String secondLastName) {
    this.secondLastName = secondLastName;
  }

  public Model27 email(String email) {
    this.email = email;
    return this;
  }

   /**
   * Email address
   * @return email
  **/
  @ApiModelProperty(value = "Email address")
  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public Model27 personRegistrations(Model26 personRegistrations) {
    this.personRegistrations = personRegistrations;
    return this;
  }

   /**
   * Get personRegistrations
   * @return personRegistrations
  **/
  @ApiModelProperty(value = "")
  public Model26 getPersonRegistrations() {
    return personRegistrations;
  }

  public void setPersonRegistrations(Model26 personRegistrations) {
    this.personRegistrations = personRegistrations;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Model27 model27 = (Model27) o;
    return Objects.equals(this.title, model27.title) &&
        Objects.equals(this.firstName, model27.firstName) &&
        Objects.equals(this.lastName, model27.lastName) &&
        Objects.equals(this.secondLastName, model27.secondLastName) &&
        Objects.equals(this.email, model27.email) &&
        Objects.equals(this.personRegistrations, model27.personRegistrations);
  }

  @Override
  public int hashCode() {
    return Objects.hash(title, firstName, lastName, secondLastName, email, personRegistrations);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Model27 {\n");
    
    sb.append("    title: ").append(toIndentedString(title)).append("\n");
    sb.append("    firstName: ").append(toIndentedString(firstName)).append("\n");
    sb.append("    lastName: ").append(toIndentedString(lastName)).append("\n");
    sb.append("    secondLastName: ").append(toIndentedString(secondLastName)).append("\n");
    sb.append("    email: ").append(toIndentedString(email)).append("\n");
    sb.append("    personRegistrations: ").append(toIndentedString(personRegistrations)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

