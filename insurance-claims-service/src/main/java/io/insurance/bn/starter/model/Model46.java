/*
 * API documentation
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: 1.0.0-20181108155220-20181108155343
 * Contact: api.move@axa-assistance.com
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */


package io.insurance.bn.starter.model;

import com.google.gson.annotations.SerializedName;
import io.swagger.annotations.ApiModelProperty;

import java.util.Objects;

/**
 * Model46
 */
@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaClientCodegen", date = "2018-12-19T23:23:06.995+01:00")
public class Model46 {
  @SerializedName("policy_holder_id")
  private String policyHolderId = null;

  @SerializedName("reference")
  private String reference = null;

  @SerializedName("title")
  private String title = null;

  @SerializedName("last_name")
  private String lastName = null;

  @SerializedName("first_name")
  private String firstName = null;

  @SerializedName("address")
  private String address = null;

  @SerializedName("postal_code")
  private String postalCode = null;

  @SerializedName("city")
  private String city = null;

  @SerializedName("country")
  private String country = null;

  @SerializedName("phone_number")
  private String phoneNumber = null;

  @SerializedName("mobile_phone_number")
  private String mobilePhoneNumber = null;

  @SerializedName("email")
  private String email = null;

  public Model46 policyHolderId(String policyHolderId) {
    this.policyHolderId = policyHolderId;
    return this;
  }

   /**
   * Policy holder&#39;s identifier
   * @return policyHolderId
  **/
  @ApiModelProperty(value = "Policy holder's identifier")
  public String getPolicyHolderId() {
    return policyHolderId;
  }

  public void setPolicyHolderId(String policyHolderId) {
    this.policyHolderId = policyHolderId;
  }

  public Model46 reference(String reference) {
    this.reference = reference;
    return this;
  }

   /**
   * Contract number of the policy holder
   * @return reference
  **/
  @ApiModelProperty(example = "0123456789", value = "Contract number of the policy holder")
  public String getReference() {
    return reference;
  }

  public void setReference(String reference) {
    this.reference = reference;
  }

  public Model46 title(String title) {
    this.title = title;
    return this;
  }

   /**
   * Policy holder&#39;s title. Expected values (MR, MRS, MISS, DR, PR)
   * @return title
  **/
  @ApiModelProperty(example = "MR", value = "Policy holder's title. Expected values (MR, MRS, MISS, DR, PR)")
  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public Model46 lastName(String lastName) {
    this.lastName = lastName;
    return this;
  }

   /**
   * Policy holder&#39;s last name
   * @return lastName
  **/
  @ApiModelProperty(example = "Doe", value = "Policy holder's last name")
  public String getLastName() {
    return lastName;
  }

  public void setLastName(String lastName) {
    this.lastName = lastName;
  }

  public Model46 firstName(String firstName) {
    this.firstName = firstName;
    return this;
  }

   /**
   * Policy holder&#39;s first name
   * @return firstName
  **/
  @ApiModelProperty(example = "John", value = "Policy holder's first name")
  public String getFirstName() {
    return firstName;
  }

  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }

  public Model46 address(String address) {
    this.address = address;
    return this;
  }

   /**
   * Policy holder&#39;s complete address
   * @return address
  **/
  @ApiModelProperty(example = "dummy address", value = "Policy holder's complete address")
  public String getAddress() {
    return address;
  }

  public void setAddress(String address) {
    this.address = address;
  }

  public Model46 postalCode(String postalCode) {
    this.postalCode = postalCode;
    return this;
  }

   /**
   * Policy holder&#39;s postal code
   * @return postalCode
  **/
  @ApiModelProperty(example = "75001", value = "Policy holder's postal code")
  public String getPostalCode() {
    return postalCode;
  }

  public void setPostalCode(String postalCode) {
    this.postalCode = postalCode;
  }

  public Model46 city(String city) {
    this.city = city;
    return this;
  }

   /**
   * Policy holder&#39;s city
   * @return city
  **/
  @ApiModelProperty(example = "paris", value = "Policy holder's city")
  public String getCity() {
    return city;
  }

  public void setCity(String city) {
    this.city = city;
  }

  public Model46 country(String country) {
    this.country = country;
    return this;
  }

   /**
   * Policy holder&#39;s country, ISO-3166-1 alpha-2 format
   * @return country
  **/
  @ApiModelProperty(example = "FR", value = "Policy holder's country, ISO-3166-1 alpha-2 format")
  public String getCountry() {
    return country;
  }

  public void setCountry(String country) {
    this.country = country;
  }

  public Model46 phoneNumber(String phoneNumber) {
    this.phoneNumber = phoneNumber;
    return this;
  }

   /**
   * Policy holder&#39;s phone number
   * @return phoneNumber
  **/
  @ApiModelProperty(example = "+34698765432", value = "Policy holder's phone number")
  public String getPhoneNumber() {
    return phoneNumber;
  }

  public void setPhoneNumber(String phoneNumber) {
    this.phoneNumber = phoneNumber;
  }

  public Model46 mobilePhoneNumber(String mobilePhoneNumber) {
    this.mobilePhoneNumber = mobilePhoneNumber;
    return this;
  }

   /**
   * Policy holder&#39;s mobile phone number
   * @return mobilePhoneNumber
  **/
  @ApiModelProperty(example = "+34600000000", value = "Policy holder's mobile phone number")
  public String getMobilePhoneNumber() {
    return mobilePhoneNumber;
  }

  public void setMobilePhoneNumber(String mobilePhoneNumber) {
    this.mobilePhoneNumber = mobilePhoneNumber;
  }

  public Model46 email(String email) {
    this.email = email;
    return this;
  }

   /**
   * Policy holder&#39;s email
   * @return email
  **/
  @ApiModelProperty(example = "john.doe@dummy.com", value = "Policy holder's email")
  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Model46 model46 = (Model46) o;
    return Objects.equals(this.policyHolderId, model46.policyHolderId) &&
        Objects.equals(this.reference, model46.reference) &&
        Objects.equals(this.title, model46.title) &&
        Objects.equals(this.lastName, model46.lastName) &&
        Objects.equals(this.firstName, model46.firstName) &&
        Objects.equals(this.address, model46.address) &&
        Objects.equals(this.postalCode, model46.postalCode) &&
        Objects.equals(this.city, model46.city) &&
        Objects.equals(this.country, model46.country) &&
        Objects.equals(this.phoneNumber, model46.phoneNumber) &&
        Objects.equals(this.mobilePhoneNumber, model46.mobilePhoneNumber) &&
        Objects.equals(this.email, model46.email);
  }

  @Override
  public int hashCode() {
    return Objects.hash(policyHolderId, reference, title, lastName, firstName, address, postalCode, city, country, phoneNumber, mobilePhoneNumber, email);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Model46 {\n");
    
    sb.append("    policyHolderId: ").append(toIndentedString(policyHolderId)).append("\n");
    sb.append("    reference: ").append(toIndentedString(reference)).append("\n");
    sb.append("    title: ").append(toIndentedString(title)).append("\n");
    sb.append("    lastName: ").append(toIndentedString(lastName)).append("\n");
    sb.append("    firstName: ").append(toIndentedString(firstName)).append("\n");
    sb.append("    address: ").append(toIndentedString(address)).append("\n");
    sb.append("    postalCode: ").append(toIndentedString(postalCode)).append("\n");
    sb.append("    city: ").append(toIndentedString(city)).append("\n");
    sb.append("    country: ").append(toIndentedString(country)).append("\n");
    sb.append("    phoneNumber: ").append(toIndentedString(phoneNumber)).append("\n");
    sb.append("    mobilePhoneNumber: ").append(toIndentedString(mobilePhoneNumber)).append("\n");
    sb.append("    email: ").append(toIndentedString(email)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

