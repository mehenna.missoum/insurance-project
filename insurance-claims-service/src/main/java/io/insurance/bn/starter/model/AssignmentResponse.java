/*
 * API documentation
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: 1.0.0-20181108155220-20181108155343
 * Contact: api.move@axa-assistance.com
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */


package io.insurance.bn.starter.model;

import com.google.gson.annotations.SerializedName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.util.Objects;

/**
 * Assignment information.
 */
@ApiModel(description = "Assignment information.")
@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaClientCodegen", date = "2018-12-19T23:23:06.995+01:00")
public class AssignmentResponse {
  @SerializedName("assignment_id")
  private String assignmentId = null;

  @SerializedName("provider_id")
  private String providerId = null;

  @SerializedName("customer_id")
  private String customerId = null;

  @SerializedName("quote_id")
  private String quoteId = null;

  @SerializedName("service")
  private String service = null;

  @SerializedName("place_type")
  private String placeType = null;

  @SerializedName("status")
  private String status = null;

  @SerializedName("vehicle")
  private Vehicle vehicle = null;

  @SerializedName("current_location")
  private Model21 currentLocation = null;

  @SerializedName("destination_location")
  private Model22 destinationLocation = null;

  @SerializedName("technician")
  private Technician technician = null;

  @SerializedName("to_schedule_on")
  private String toScheduleOn = null;

  @SerializedName("expected_on")
  private String expectedOn = null;

  public AssignmentResponse assignmentId(String assignmentId) {
    this.assignmentId = assignmentId;
    return this;
  }

   /**
   * Assignment unique identifier.
   * @return assignmentId
  **/
  @ApiModelProperty(required = true, value = "Assignment unique identifier.")
  public String getAssignmentId() {
    return assignmentId;
  }

  public void setAssignmentId(String assignmentId) {
    this.assignmentId = assignmentId;
  }

  public AssignmentResponse providerId(String providerId) {
    this.providerId = providerId;
    return this;
  }

   /**
   * Provider unique identifier.
   * @return providerId
  **/
  @ApiModelProperty(required = true, value = "Provider unique identifier.")
  public String getProviderId() {
    return providerId;
  }

  public void setProviderId(String providerId) {
    this.providerId = providerId;
  }

  public AssignmentResponse customerId(String customerId) {
    this.customerId = customerId;
    return this;
  }

   /**
   * Customer unique identifier.
   * @return customerId
  **/
  @ApiModelProperty(value = "Customer unique identifier.")
  public String getCustomerId() {
    return customerId;
  }

  public void setCustomerId(String customerId) {
    this.customerId = customerId;
  }

  public AssignmentResponse quoteId(String quoteId) {
    this.quoteId = quoteId;
    return this;
  }

   /**
   * Quote identifier.
   * @return quoteId
  **/
  @ApiModelProperty(value = "Quote identifier.")
  public String getQuoteId() {
    return quoteId;
  }

  public void setQuoteId(String quoteId) {
    this.quoteId = quoteId;
  }

  public AssignmentResponse service(String service) {
    this.service = service;
    return this;
  }

   /**
   * The requested service. Expected values: \&quot;TOWING\&quot;, \&quot;CRANING\&quot;, \&quot;ROADSIDE_REPAIR\&quot;
   * @return service
  **/
  @ApiModelProperty(example = "TOWING", value = "The requested service. Expected values: \"TOWING\", \"CRANING\", \"ROADSIDE_REPAIR\"")
  public String getService() {
    return service;
  }

  public void setService(String service) {
    this.service = service;
  }

  public AssignmentResponse placeType(String placeType) {
    this.placeType = placeType;
    return this;
  }

   /**
   * Place type where the vehicle is parked. Expect values: \&quot;BASEMENT\&quot;, \&quot;HIGHWAY\&quot;, \&quot;MULTISTOREY_PARK\&quot;, \&quot;OTHER\&quot;.
   * @return placeType
  **/
  @ApiModelProperty(example = "BASEMENT", value = "Place type where the vehicle is parked. Expect values: \"BASEMENT\", \"HIGHWAY\", \"MULTISTOREY_PARK\", \"OTHER\".")
  public String getPlaceType() {
    return placeType;
  }

  public void setPlaceType(String placeType) {
    this.placeType = placeType;
  }

  public AssignmentResponse status(String status) {
    this.status = status;
    return this;
  }

   /**
   * Status of assignment [ACCEPTED : The provider has accepted the service; CANCELLED : The assignment has been cancelled; CLOSED : The assignment is closed, there is no pending tasks any more linked to it; COMPLETED : The provider has completed the service (some administrative actions can be added); CREATED : The assignment is created; PLANNED : Planned in the provider calendar (ie medical consultation planned in the doctor agenda); REFUSED : The provider has refused the service; SELECTED : The provider has been selected for the mission; STARTED : The assignment has started. It’s in progress; SENT : The provider has been sent to the provider for validation; TIMEOUT : The assignment hasn&#39;t received acceptance or refusal and is in timeout].
   * @return status
  **/
  @ApiModelProperty(example = "TIMEOUT", required = true, value = "Status of assignment [ACCEPTED : The provider has accepted the service; CANCELLED : The assignment has been cancelled; CLOSED : The assignment is closed, there is no pending tasks any more linked to it; COMPLETED : The provider has completed the service (some administrative actions can be added); CREATED : The assignment is created; PLANNED : Planned in the provider calendar (ie medical consultation planned in the doctor agenda); REFUSED : The provider has refused the service; SELECTED : The provider has been selected for the mission; STARTED : The assignment has started. It’s in progress; SENT : The provider has been sent to the provider for validation; TIMEOUT : The assignment hasn't received acceptance or refusal and is in timeout].")
  public String getStatus() {
    return status;
  }

  public void setStatus(String status) {
    this.status = status;
  }

  public AssignmentResponse vehicle(Vehicle vehicle) {
    this.vehicle = vehicle;
    return this;
  }

   /**
   * Get vehicle
   * @return vehicle
  **/
  @ApiModelProperty(value = "")
  public Vehicle getVehicle() {
    return vehicle;
  }

  public void setVehicle(Vehicle vehicle) {
    this.vehicle = vehicle;
  }

  public AssignmentResponse currentLocation(Model21 currentLocation) {
    this.currentLocation = currentLocation;
    return this;
  }

   /**
   * Get currentLocation
   * @return currentLocation
  **/
  @ApiModelProperty(required = true, value = "")
  public Model21 getCurrentLocation() {
    return currentLocation;
  }

  public void setCurrentLocation(Model21 currentLocation) {
    this.currentLocation = currentLocation;
  }

  public AssignmentResponse destinationLocation(Model22 destinationLocation) {
    this.destinationLocation = destinationLocation;
    return this;
  }

   /**
   * Get destinationLocation
   * @return destinationLocation
  **/
  @ApiModelProperty(value = "")
  public Model22 getDestinationLocation() {
    return destinationLocation;
  }

  public void setDestinationLocation(Model22 destinationLocation) {
    this.destinationLocation = destinationLocation;
  }

  public AssignmentResponse technician(Technician technician) {
    this.technician = technician;
    return this;
  }

   /**
   * Get technician
   * @return technician
  **/
  @ApiModelProperty(value = "")
  public Technician getTechnician() {
    return technician;
  }

  public void setTechnician(Technician technician) {
    this.technician = technician;
  }

  public AssignmentResponse toScheduleOn(String toScheduleOn) {
    this.toScheduleOn = toScheduleOn;
    return this;
  }

   /**
   * Date and time requested by the customer to schedule the service - UTC datetime, RFC3339 format (YYYY-MM-DDTHH:mmZ).
   * @return toScheduleOn
  **/
  @ApiModelProperty(value = "Date and time requested by the customer to schedule the service - UTC datetime, RFC3339 format (YYYY-MM-DDTHH:mmZ).")
  public String getToScheduleOn() {
    return toScheduleOn;
  }

  public void setToScheduleOn(String toScheduleOn) {
    this.toScheduleOn = toScheduleOn;
  }

  public AssignmentResponse expectedOn(String expectedOn) {
    this.expectedOn = expectedOn;
    return this;
  }

   /**
   * Date and time when the service delivery is expected - UTC datetime, RFC3339 format (YYYY-MM-DDTHH:mmZ).
   * @return expectedOn
  **/
  @ApiModelProperty(value = "Date and time when the service delivery is expected - UTC datetime, RFC3339 format (YYYY-MM-DDTHH:mmZ).")
  public String getExpectedOn() {
    return expectedOn;
  }

  public void setExpectedOn(String expectedOn) {
    this.expectedOn = expectedOn;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    AssignmentResponse assignmentResponse = (AssignmentResponse) o;
    return Objects.equals(this.assignmentId, assignmentResponse.assignmentId) &&
        Objects.equals(this.providerId, assignmentResponse.providerId) &&
        Objects.equals(this.customerId, assignmentResponse.customerId) &&
        Objects.equals(this.quoteId, assignmentResponse.quoteId) &&
        Objects.equals(this.service, assignmentResponse.service) &&
        Objects.equals(this.placeType, assignmentResponse.placeType) &&
        Objects.equals(this.status, assignmentResponse.status) &&
        Objects.equals(this.vehicle, assignmentResponse.vehicle) &&
        Objects.equals(this.currentLocation, assignmentResponse.currentLocation) &&
        Objects.equals(this.destinationLocation, assignmentResponse.destinationLocation) &&
        Objects.equals(this.technician, assignmentResponse.technician) &&
        Objects.equals(this.toScheduleOn, assignmentResponse.toScheduleOn) &&
        Objects.equals(this.expectedOn, assignmentResponse.expectedOn);
  }

  @Override
  public int hashCode() {
    return Objects.hash(assignmentId, providerId, customerId, quoteId, service, placeType, status, vehicle, currentLocation, destinationLocation, technician, toScheduleOn, expectedOn);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class AssignmentResponse {\n");
    
    sb.append("    assignmentId: ").append(toIndentedString(assignmentId)).append("\n");
    sb.append("    providerId: ").append(toIndentedString(providerId)).append("\n");
    sb.append("    customerId: ").append(toIndentedString(customerId)).append("\n");
    sb.append("    quoteId: ").append(toIndentedString(quoteId)).append("\n");
    sb.append("    service: ").append(toIndentedString(service)).append("\n");
    sb.append("    placeType: ").append(toIndentedString(placeType)).append("\n");
    sb.append("    status: ").append(toIndentedString(status)).append("\n");
    sb.append("    vehicle: ").append(toIndentedString(vehicle)).append("\n");
    sb.append("    currentLocation: ").append(toIndentedString(currentLocation)).append("\n");
    sb.append("    destinationLocation: ").append(toIndentedString(destinationLocation)).append("\n");
    sb.append("    technician: ").append(toIndentedString(technician)).append("\n");
    sb.append("    toScheduleOn: ").append(toIndentedString(toScheduleOn)).append("\n");
    sb.append("    expectedOn: ").append(toIndentedString(expectedOn)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

