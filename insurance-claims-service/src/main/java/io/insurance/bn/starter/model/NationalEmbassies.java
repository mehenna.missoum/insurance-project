/*
 * API documentation
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: 1.0.0-20181108155220-20181108155343
 * Contact: api.move@axa-assistance.com
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */


package io.insurance.bn.starter.model;

import com.google.gson.annotations.SerializedName;
import io.swagger.annotations.ApiModelProperty;

import java.util.List;
import java.util.Objects;

/**
 * NationalEmbassies
 */
@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaClientCodegen", date = "2018-12-19T23:23:06.995+01:00")
public class NationalEmbassies {
  @SerializedName("label")
  private String label = null;

  @SerializedName("list")
  private List list = null;

  public NationalEmbassies label(String label) {
    this.label = label;
    return this;
  }

   /**
   * National Embassies label. Ex: \&quot;National Embassies\&quot;
   * @return label
  **/
  @ApiModelProperty(value = "National Embassies label. Ex: \"National Embassies\"")
  public String getLabel() {
    return label;
  }

  public void setLabel(String label) {
    this.label = label;
  }

  public NationalEmbassies list(List list) {
    this.list = list;
    return this;
  }

   /**
   * Get list
   * @return list
  **/
  @ApiModelProperty(value = "")
  public List getList() {
    return list;
  }

  public void setList(List list) {
    this.list = list;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    NationalEmbassies nationalEmbassies = (NationalEmbassies) o;
    return Objects.equals(this.label, nationalEmbassies.label) &&
        Objects.equals(this.list, nationalEmbassies.list);
  }

  @Override
  public int hashCode() {
    return Objects.hash(label, list);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class NationalEmbassies {\n");
    
    sb.append("    label: ").append(toIndentedString(label)).append("\n");
    sb.append("    list: ").append(toIndentedString(list)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

