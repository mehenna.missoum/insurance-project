/*
 * API documentation
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: 1.0.0-20181108155220-20181108155343
 * Contact: api.move@axa-assistance.com
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */


package io.insurance.bn.starter.model;

import java.util.Objects;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.IOException;
import java.math.BigDecimal;

/**
 * Device criteria.
 */
@ApiModel(description = "Device criteria.")
@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaClientCodegen", date = "2018-12-19T23:23:06.995+01:00")
public class DeviceCriteria {
  @SerializedName("categorization_level_1")
  private String categorizationLevel1 = null;

  @SerializedName("categorization_level_2")
  private String categorizationLevel2 = null;

  @SerializedName("brand")
  private String brand = null;

  @SerializedName("model")
  private String model = null;

  @SerializedName("purchase_price")
  private BigDecimal purchasePrice = null;

  @SerializedName("purchase_date")
  private String purchaseDate = null;

  @SerializedName("is_purchased_new")
  private Boolean isPurchasedNew = null;

  public DeviceCriteria categorizationLevel1(String categorizationLevel1) {
    this.categorizationLevel1 = categorizationLevel1;
    return this;
  }

   /**
   * First level of device categorization. Expected values (WHITE, BROWN, GREY, GREEN, MOBILE_CONNECT_GADGET, DIY)
   * @return categorizationLevel1
  **/
  @ApiModelProperty(example = "WHITE", required = true, value = "First level of device categorization. Expected values (WHITE, BROWN, GREY, GREEN, MOBILE_CONNECT_GADGET, DIY)")
  public String getCategorizationLevel1() {
    return categorizationLevel1;
  }

  public void setCategorizationLevel1(String categorizationLevel1) {
    this.categorizationLevel1 = categorizationLevel1;
  }

  public DeviceCriteria categorizationLevel2(String categorizationLevel2) {
    this.categorizationLevel2 = categorizationLevel2;
    return this;
  }

   /**
   * Second level of device categorization (HEAVY APPLIANCE, SMALL APPLIANCE, VIDEO, SMARTPHONE ...)
   * @return categorizationLevel2
  **/
  @ApiModelProperty(example = "SMARTPHONE", value = "Second level of device categorization (HEAVY APPLIANCE, SMALL APPLIANCE, VIDEO, SMARTPHONE ...)")
  public String getCategorizationLevel2() {
    return categorizationLevel2;
  }

  public void setCategorizationLevel2(String categorizationLevel2) {
    this.categorizationLevel2 = categorizationLevel2;
  }

  public DeviceCriteria brand(String brand) {
    this.brand = brand;
    return this;
  }

   /**
   * Brand
   * @return brand
  **/
  @ApiModelProperty(example = "APPLE", value = "Brand")
  public String getBrand() {
    return brand;
  }

  public void setBrand(String brand) {
    this.brand = brand;
  }

  public DeviceCriteria model(String model) {
    this.model = model;
    return this;
  }

   /**
   * Model.
   * @return model
  **/
  @ApiModelProperty(example = "IPHONE X", required = true, value = "Model.")
  public String getModel() {
    return model;
  }

  public void setModel(String model) {
    this.model = model;
  }

  public DeviceCriteria purchasePrice(BigDecimal purchasePrice) {
    this.purchasePrice = purchasePrice;
    return this;
  }

   /**
   * Total amount of purchase price, numeric with 2 decimals.
   * @return purchasePrice
  **/
  @ApiModelProperty(example = "950.0", required = true, value = "Total amount of purchase price, numeric with 2 decimals.")
  public BigDecimal getPurchasePrice() {
    return purchasePrice;
  }

  public void setPurchasePrice(BigDecimal purchasePrice) {
    this.purchasePrice = purchasePrice;
  }

  public DeviceCriteria purchaseDate(String purchaseDate) {
    this.purchaseDate = purchaseDate;
    return this;
  }

   /**
   * Date of the purchase - ISO 8601 format (YYYY-MM-DD).
   * @return purchaseDate
  **/
  @ApiModelProperty(example = "2018-11-20", required = true, value = "Date of the purchase - ISO 8601 format (YYYY-MM-DD).")
  public String getPurchaseDate() {
    return purchaseDate;
  }

  public void setPurchaseDate(String purchaseDate) {
    this.purchaseDate = purchaseDate;
  }

  public DeviceCriteria isPurchasedNew(Boolean isPurchasedNew) {
    this.isPurchasedNew = isPurchasedNew;
    return this;
  }

   /**
   * Indicates if the device is purchased new.
   * @return isPurchasedNew
  **/
  @ApiModelProperty(value = "Indicates if the device is purchased new.")
  public Boolean isIsPurchasedNew() {
    return isPurchasedNew;
  }

  public void setIsPurchasedNew(Boolean isPurchasedNew) {
    this.isPurchasedNew = isPurchasedNew;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    DeviceCriteria deviceCriteria = (DeviceCriteria) o;
    return Objects.equals(this.categorizationLevel1, deviceCriteria.categorizationLevel1) &&
        Objects.equals(this.categorizationLevel2, deviceCriteria.categorizationLevel2) &&
        Objects.equals(this.brand, deviceCriteria.brand) &&
        Objects.equals(this.model, deviceCriteria.model) &&
        Objects.equals(this.purchasePrice, deviceCriteria.purchasePrice) &&
        Objects.equals(this.purchaseDate, deviceCriteria.purchaseDate) &&
        Objects.equals(this.isPurchasedNew, deviceCriteria.isPurchasedNew);
  }

  @Override
  public int hashCode() {
    return Objects.hash(categorizationLevel1, categorizationLevel2, brand, model, purchasePrice, purchaseDate, isPurchasedNew);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class DeviceCriteria {\n");
    
    sb.append("    categorizationLevel1: ").append(toIndentedString(categorizationLevel1)).append("\n");
    sb.append("    categorizationLevel2: ").append(toIndentedString(categorizationLevel2)).append("\n");
    sb.append("    brand: ").append(toIndentedString(brand)).append("\n");
    sb.append("    model: ").append(toIndentedString(model)).append("\n");
    sb.append("    purchasePrice: ").append(toIndentedString(purchasePrice)).append("\n");
    sb.append("    purchaseDate: ").append(toIndentedString(purchaseDate)).append("\n");
    sb.append("    isPurchasedNew: ").append(toIndentedString(isPurchasedNew)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

