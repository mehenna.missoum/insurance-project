package io.insurance.bn.starter.model;

import com.google.gson.annotations.SerializedName;

public class Model300 {

    @SerializedName("claim_type_id")
    private String claimTypeId = null;
    @SerializedName("name")
    private String name = null;
    @SerializedName("certificate_type_name")
    private String certificateTypeName = null;


    public String getClaimTypeId() {
        return claimTypeId;
    }

    public void setClaimTypeId(String claimTypeId) {
        this.claimTypeId = claimTypeId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCertificateTypeName() {
        return certificateTypeName;
    }

    public void setCertificateTypeName(String certificateTypeName) {
        this.certificateTypeName = certificateTypeName;
    }
}
