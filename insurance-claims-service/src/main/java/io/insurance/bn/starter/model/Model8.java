/*
 * API documentation
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: 1.0.0-20181108155220-20181108155343
 * Contact: api.move@axa-assistance.com
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */


package io.insurance.bn.starter.model;

import com.google.gson.annotations.SerializedName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.util.Objects;

/**
 * Claim&#39;s informations.
 */
@ApiModel(description = "Claim's informations.")
@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaClientCodegen", date = "2018-12-19T23:23:06.995+01:00")
public class Model8 {
  @SerializedName("claim_id")
  private String claimId = null;

  @SerializedName("claim_type_name")
  private String claimTypeName = null;

  @SerializedName("attachments")
  private Attachments attachments = null;

  @SerializedName("existing_case_number")
  private String existingCaseNumber = null;

  @SerializedName("statuses")
  private Statuses statuses = null;

  @SerializedName("product_criteria")
  private ProductCriteria productCriteria = null;

  @SerializedName("amount_claimed")
  private Amount amountClaimed = null;

  public Model8 claimId(String claimId) {
    this.claimId = claimId;
    return this;
  }

   /**
   * Identifier of the claim.
   * @return claimId
  **/
  @ApiModelProperty(required = true, value = "Identifier of the claim.")
  public String getClaimId() {
    return claimId;
  }

  public void setClaimId(String claimId) {
    this.claimId = claimId;
  }

  public Model8 claimTypeName(String claimTypeName) {
    this.claimTypeName = claimTypeName;
    return this;
  }

   /**
   * Claim type name.
   * @return claimTypeName
  **/
  @ApiModelProperty(example = "car rental", value = "Claim type name.")
  public String getClaimTypeName() {
    return claimTypeName;
  }

  public void setClaimTypeName(String claimTypeName) {
    this.claimTypeName = claimTypeName;
  }

  public Model8 attachments(Attachments attachments) {
    this.attachments = attachments;
    return this;
  }

   /**
   * Get attachments
   * @return attachments
  **/
  @ApiModelProperty(value = "")
  public Attachments getAttachments() {
    return attachments;
  }

  public void setAttachments(Attachments attachments) {
    this.attachments = attachments;
  }

  public Model8 existingCaseNumber(String existingCaseNumber) {
    this.existingCaseNumber = existingCaseNumber;
    return this;
  }

   /**
   * Case number created when the customer declared his sinister via another communication channel (phone).
   * @return existingCaseNumber
  **/
  @ApiModelProperty(value = "Case number created when the customer declared his sinister via another communication channel (phone).")
  public String getExistingCaseNumber() {
    return existingCaseNumber;
  }

  public void setExistingCaseNumber(String existingCaseNumber) {
    this.existingCaseNumber = existingCaseNumber;
  }

  public Model8 statuses(Statuses statuses) {
    this.statuses = statuses;
    return this;
  }

   /**
   * Get statuses
   * @return statuses
  **/
  @ApiModelProperty(value = "")
  public Statuses getStatuses() {
    return statuses;
  }

  public void setStatuses(Statuses statuses) {
    this.statuses = statuses;
  }

  public Model8 productCriteria(ProductCriteria productCriteria) {
    this.productCriteria = productCriteria;
    return this;
  }

   /**
   * Get productCriteria
   * @return productCriteria
  **/
  @ApiModelProperty(value = "")
  public ProductCriteria getProductCriteria() {
    return productCriteria;
  }

  public void setProductCriteria(ProductCriteria productCriteria) {
    this.productCriteria = productCriteria;
  }

  public Model8 amountClaimed(Amount amountClaimed) {
    this.amountClaimed = amountClaimed;
    return this;
  }

   /**
   * Get amountClaimed
   * @return amountClaimed
  **/
  @ApiModelProperty(value = "")
  public Amount getAmountClaimed() {
    return amountClaimed;
  }

  public void setAmountClaimed(Amount amountClaimed) {
    this.amountClaimed = amountClaimed;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Model8 model8 = (Model8) o;
    return Objects.equals(this.claimId, model8.claimId) &&
        Objects.equals(this.claimTypeName, model8.claimTypeName) &&
        Objects.equals(this.attachments, model8.attachments) &&
        Objects.equals(this.existingCaseNumber, model8.existingCaseNumber) &&
        Objects.equals(this.statuses, model8.statuses) &&
        Objects.equals(this.productCriteria, model8.productCriteria) &&
        Objects.equals(this.amountClaimed, model8.amountClaimed);
  }

  @Override
  public int hashCode() {
    return Objects.hash(claimId, claimTypeName, attachments, existingCaseNumber, statuses, productCriteria, amountClaimed);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Model8 {\n");
    
    sb.append("    claimId: ").append(toIndentedString(claimId)).append("\n");
    sb.append("    claimTypeName: ").append(toIndentedString(claimTypeName)).append("\n");
    sb.append("    attachments: ").append(toIndentedString(attachments)).append("\n");
    sb.append("    existingCaseNumber: ").append(toIndentedString(existingCaseNumber)).append("\n");
    sb.append("    statuses: ").append(toIndentedString(statuses)).append("\n");
    sb.append("    productCriteria: ").append(toIndentedString(productCriteria)).append("\n");
    sb.append("    amountClaimed: ").append(toIndentedString(amountClaimed)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

