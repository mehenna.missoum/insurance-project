/*
 * API documentation
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: 1.0.0-20181108155220-20181108155343
 * Contact: api.move@axa-assistance.com
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */


package io.insurance.bn.starter.model;

import com.google.gson.annotations.SerializedName;
import io.swagger.annotations.ApiModelProperty;

import java.util.Objects;

/**
 * AdditionalData
 */
@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaClientCodegen", date = "2018-12-19T23:23:06.995+01:00")
public class AdditionalData {
  @SerializedName("name")
  private String name = null;

  @SerializedName("value")
  private String value = null;

  public AdditionalData name(String name) {
    this.name = name;
    return this;
  }

   /**
   * Key used to identify this additional data (e.g.: emergency_number)
   * @return name
  **/
  @ApiModelProperty(example = "emergency_number", required = true, value = "Key used to identify this additional data (e.g.: emergency_number)")
  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public AdditionalData value(String value) {
    this.value = value;
    return this;
  }

   /**
   * Value of this additional data (e.g.: 0548469875)
   * @return value
  **/
  @ApiModelProperty(example = "123456789", required = true, value = "Value of this additional data (e.g.: 0548469875)")
  public String getValue() {
    return value;
  }

  public void setValue(String value) {
    this.value = value;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    AdditionalData additionalData = (AdditionalData) o;
    return Objects.equals(this.name, additionalData.name) &&
        Objects.equals(this.value, additionalData.value);
  }

  @Override
  public int hashCode() {
    return Objects.hash(name, value);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class AdditionalData {\n");
    
    sb.append("    name: ").append(toIndentedString(name)).append("\n");
    sb.append("    value: ").append(toIndentedString(value)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

