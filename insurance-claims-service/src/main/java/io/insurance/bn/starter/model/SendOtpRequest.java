/*
 * API documentation
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: 1.0.0-20181108155220-20181108155343
 * Contact: api.move@axa-assistance.com
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */


package io.insurance.bn.starter.model;

import com.google.gson.annotations.SerializedName;
import io.swagger.annotations.ApiModelProperty;

import java.util.Objects;

/**
 * SendOtpRequest
 */
@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaClientCodegen", date = "2018-12-19T23:23:06.995+01:00")
public class SendOtpRequest {
  @SerializedName("confirmation_id")
  private String confirmationId = null;

  @SerializedName("phone_number")
  private String phoneNumber = null;

  public SendOtpRequest confirmationId(String confirmationId) {
    this.confirmationId = confirmationId;
    return this;
  }

   /**
   * Confirmation id
   * @return confirmationId
  **/
  @ApiModelProperty(required = true, value = "Confirmation id")
  public String getConfirmationId() {
    return confirmationId;
  }

  public void setConfirmationId(String confirmationId) {
    this.confirmationId = confirmationId;
  }

  public SendOtpRequest phoneNumber(String phoneNumber) {
    this.phoneNumber = phoneNumber;
    return this;
  }

   /**
   * Phone number
   * @return phoneNumber
  **/
  @ApiModelProperty(example = "+34612589741", required = true, value = "Phone number")
  public String getPhoneNumber() {
    return phoneNumber;
  }

  public void setPhoneNumber(String phoneNumber) {
    this.phoneNumber = phoneNumber;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    SendOtpRequest sendOtpRequest = (SendOtpRequest) o;
    return Objects.equals(this.confirmationId, sendOtpRequest.confirmationId) &&
        Objects.equals(this.phoneNumber, sendOtpRequest.phoneNumber);
  }

  @Override
  public int hashCode() {
    return Objects.hash(confirmationId, phoneNumber);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class SendOtpRequest {\n");
    
    sb.append("    confirmationId: ").append(toIndentedString(confirmationId)).append("\n");
    sb.append("    phoneNumber: ").append(toIndentedString(phoneNumber)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

