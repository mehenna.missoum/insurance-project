/*
 * API documentation
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: 1.0.0-20181108155220-20181108155343
 * Contact: api.move@axa-assistance.com
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */


package io.insurance.bn.starter.model;

import com.google.gson.annotations.SerializedName;
import io.swagger.annotations.ApiModelProperty;

import java.util.Objects;

/**
 * CarRentalClaimReimbursementRequest
 */
@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaClientCodegen", date = "2018-12-19T23:23:06.995+01:00")
public class CarRentalClaimReimbursementRequest {
  @SerializedName("banking_information")
  private Model253 bankingInformation = null;

  public CarRentalClaimReimbursementRequest bankingInformation(Model253 bankingInformation) {
    this.bankingInformation = bankingInformation;
    return this;
  }

   /**
   * Get bankingInformation
   * @return bankingInformation
  **/
  @ApiModelProperty(required = true, value = "")
  public Model253 getBankingInformation() {
    return bankingInformation;
  }

  public void setBankingInformation(Model253 bankingInformation) {
    this.bankingInformation = bankingInformation;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    CarRentalClaimReimbursementRequest carRentalClaimReimbursementRequest = (CarRentalClaimReimbursementRequest) o;
    return Objects.equals(this.bankingInformation, carRentalClaimReimbursementRequest.bankingInformation);
  }

  @Override
  public int hashCode() {
    return Objects.hash(bankingInformation);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class CarRentalClaimReimbursementRequest {\n");
    
    sb.append("    bankingInformation: ").append(toIndentedString(bankingInformation)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

