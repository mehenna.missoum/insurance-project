/*
 * API documentation
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: 1.0.0-20181108155220-20181108155343
 * Contact: api.move@axa-assistance.com
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */


package io.insurance.bn.starter.model;

import com.google.gson.annotations.SerializedName;
import io.swagger.annotations.ApiModelProperty;

import java.util.Objects;

/**
 * Model135
 */
@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaClientCodegen", date = "2018-12-19T23:23:06.995+01:00")
public class Model135 {
  @SerializedName("customer_id")
  private String customerId = null;

  @SerializedName("quote_id")
  private String quoteId = null;

  @SerializedName("service")
  private String service = null;

  @SerializedName("cause")
  private String cause = null;

  @SerializedName("place_type")
  private String placeType = null;

  @SerializedName("vehicle")
  private Model24 vehicle = null;

  @SerializedName("is_payer_the_customer")
  private Boolean isPayerTheCustomer = null;

  @SerializedName("payer")
  private Payer payer = null;

  @SerializedName("current_location")
  private Model30 currentLocation = null;

  @SerializedName("destination_location")
  private Model30 destinationLocation = null;

  @SerializedName("to_schedule_on")
  private String toScheduleOn = null;

  public Model135 customerId(String customerId) {
    this.customerId = customerId;
    return this;
  }

   /**
   * Customer unique identifier
   * @return customerId
  **/
  @ApiModelProperty(required = true, value = "Customer unique identifier")
  public String getCustomerId() {
    return customerId;
  }

  public void setCustomerId(String customerId) {
    this.customerId = customerId;
  }

  public Model135 quoteId(String quoteId) {
    this.quoteId = quoteId;
    return this;
  }

   /**
   * Quote identifier
   * @return quoteId
  **/
  @ApiModelProperty(value = "Quote identifier")
  public String getQuoteId() {
    return quoteId;
  }

  public void setQuoteId(String quoteId) {
    this.quoteId = quoteId;
  }

  public Model135 service(String service) {
    this.service = service;
    return this;
  }

   /**
   * Requested service. Expected values (TOWING, CRANING, ROADSIDE_REPAIR)
   * @return service
  **/
  @ApiModelProperty(example = "TOWING", required = true, value = "Requested service. Expected values (TOWING, CRANING, ROADSIDE_REPAIR)")
  public String getService() {
    return service;
  }

  public void setService(String service) {
    this.service = service;
  }

  public Model135 cause(String cause) {
    this.cause = cause;
    return this;
  }

   /**
   * Cause of the vehicle issue. Expected values (ACCIDENT, STARTING_ISSUE, MECHANIC_BREAKDOWN, NO_FUEL, TIRES, OTHERS)
   * @return cause
  **/
  @ApiModelProperty(example = "MECHANIC_BREAKDOWN", value = "Cause of the vehicle issue. Expected values (ACCIDENT, STARTING_ISSUE, MECHANIC_BREAKDOWN, NO_FUEL, TIRES, OTHERS)")
  public String getCause() {
    return cause;
  }

  public void setCause(String cause) {
    this.cause = cause;
  }

  public Model135 placeType(String placeType) {
    this.placeType = placeType;
    return this;
  }

   /**
   * Type of the place where the vehicle is parked. Expected values (BASEMENT, HIGHWAY, MULTISTOREY_PARK, OTHER)
   * @return placeType
  **/
  @ApiModelProperty(example = "BASEMENT", value = "Type of the place where the vehicle is parked. Expected values (BASEMENT, HIGHWAY, MULTISTOREY_PARK, OTHER)")
  public String getPlaceType() {
    return placeType;
  }

  public void setPlaceType(String placeType) {
    this.placeType = placeType;
  }

  public Model135 vehicle(Model24 vehicle) {
    this.vehicle = vehicle;
    return this;
  }

   /**
   * Get vehicle
   * @return vehicle
  **/
  @ApiModelProperty(value = "")
  public Model24 getVehicle() {
    return vehicle;
  }

  public void setVehicle(Model24 vehicle) {
    this.vehicle = vehicle;
  }

  public Model135 isPayerTheCustomer(Boolean isPayerTheCustomer) {
    this.isPayerTheCustomer = isPayerTheCustomer;
    return this;
  }

   /**
   * Indicates if the customer is the payer, if the value is &#39;false&#39; so the payer is mandatory
   * @return isPayerTheCustomer
  **/
  @ApiModelProperty(example = "true", value = "Indicates if the customer is the payer, if the value is 'false' so the payer is mandatory")
  public Boolean isIsPayerTheCustomer() {
    return isPayerTheCustomer;
  }

  public void setIsPayerTheCustomer(Boolean isPayerTheCustomer) {
    this.isPayerTheCustomer = isPayerTheCustomer;
  }

  public Model135 payer(Payer payer) {
    this.payer = payer;
    return this;
  }

   /**
   * Get payer
   * @return payer
  **/
  @ApiModelProperty(value = "")
  public Payer getPayer() {
    return payer;
  }

  public void setPayer(Payer payer) {
    this.payer = payer;
  }

  public Model135 currentLocation(Model30 currentLocation) {
    this.currentLocation = currentLocation;
    return this;
  }

   /**
   * Get currentLocation
   * @return currentLocation
  **/
  @ApiModelProperty(value = "")
  public Model30 getCurrentLocation() {
    return currentLocation;
  }

  public void setCurrentLocation(Model30 currentLocation) {
    this.currentLocation = currentLocation;
  }

  public Model135 destinationLocation(Model30 destinationLocation) {
    this.destinationLocation = destinationLocation;
    return this;
  }

   /**
   * Get destinationLocation
   * @return destinationLocation
  **/
  @ApiModelProperty(value = "")
  public Model30 getDestinationLocation() {
    return destinationLocation;
  }

  public void setDestinationLocation(Model30 destinationLocation) {
    this.destinationLocation = destinationLocation;
  }

  public Model135 toScheduleOn(String toScheduleOn) {
    this.toScheduleOn = toScheduleOn;
    return this;
  }

   /**
   * Date and time requested by the customer to schedule the service - UTC datetime, RFC3339 format (YYYY-MM-DDTHH:mmZ).
   * @return toScheduleOn
  **/
  @ApiModelProperty(value = "Date and time requested by the customer to schedule the service - UTC datetime, RFC3339 format (YYYY-MM-DDTHH:mmZ).")
  public String getToScheduleOn() {
    return toScheduleOn;
  }

  public void setToScheduleOn(String toScheduleOn) {
    this.toScheduleOn = toScheduleOn;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Model135 model135 = (Model135) o;
    return Objects.equals(this.customerId, model135.customerId) &&
        Objects.equals(this.quoteId, model135.quoteId) &&
        Objects.equals(this.service, model135.service) &&
        Objects.equals(this.cause, model135.cause) &&
        Objects.equals(this.placeType, model135.placeType) &&
        Objects.equals(this.vehicle, model135.vehicle) &&
        Objects.equals(this.isPayerTheCustomer, model135.isPayerTheCustomer) &&
        Objects.equals(this.payer, model135.payer) &&
        Objects.equals(this.currentLocation, model135.currentLocation) &&
        Objects.equals(this.destinationLocation, model135.destinationLocation) &&
        Objects.equals(this.toScheduleOn, model135.toScheduleOn);
  }

  @Override
  public int hashCode() {
    return Objects.hash(customerId, quoteId, service, cause, placeType, vehicle, isPayerTheCustomer, payer, currentLocation, destinationLocation, toScheduleOn);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Model135 {\n");
    
    sb.append("    customerId: ").append(toIndentedString(customerId)).append("\n");
    sb.append("    quoteId: ").append(toIndentedString(quoteId)).append("\n");
    sb.append("    service: ").append(toIndentedString(service)).append("\n");
    sb.append("    cause: ").append(toIndentedString(cause)).append("\n");
    sb.append("    placeType: ").append(toIndentedString(placeType)).append("\n");
    sb.append("    vehicle: ").append(toIndentedString(vehicle)).append("\n");
    sb.append("    isPayerTheCustomer: ").append(toIndentedString(isPayerTheCustomer)).append("\n");
    sb.append("    payer: ").append(toIndentedString(payer)).append("\n");
    sb.append("    currentLocation: ").append(toIndentedString(currentLocation)).append("\n");
    sb.append("    destinationLocation: ").append(toIndentedString(destinationLocation)).append("\n");
    sb.append("    toScheduleOn: ").append(toIndentedString(toScheduleOn)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

