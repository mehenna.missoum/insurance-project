/*
 * API documentation
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: 1.0.0-20181108155220-20181108155343
 * Contact: api.move@axa-assistance.com
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */


package io.insurance.bn.starter.model;

import com.google.gson.annotations.SerializedName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.util.Objects;

/**
 * Information related to the travel.
 */
@ApiModel(description = "Information related to the travel.")
@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaClientCodegen", date = "2018-12-19T23:23:06.995+01:00")
public class Model114 {
  @SerializedName("is_airline_compensation")
  private Boolean isAirlineCompensation = null;

  @SerializedName("form_of_payment")
  private String formOfPayment = null;

  @SerializedName("programmed_flights")
  private ProgrammedFlights programmedFlights = null;

  @SerializedName("replacement_flights")
  private ReplacementFlights replacementFlights = null;

  @SerializedName("time_lost")
  private String timeLost = null;

  @SerializedName("agency_information")
  private Model113 agencyInformation = null;

  public Model114 isAirlineCompensation(Boolean isAirlineCompensation) {
    this.isAirlineCompensation = isAirlineCompensation;
    return this;
  }

   /**
   * Does the airline offer any compensation for the missing connection?
   * @return isAirlineCompensation
  **/
  @ApiModelProperty(value = "Does the airline offer any compensation for the missing connection?")
  public Boolean isIsAirlineCompensation() {
    return isAirlineCompensation;
  }

  public void setIsAirlineCompensation(Boolean isAirlineCompensation) {
    this.isAirlineCompensation = isAirlineCompensation;
  }

  public Model114 formOfPayment(String formOfPayment) {
    this.formOfPayment = formOfPayment;
    return this;
  }

   /**
   * The purchased form of payment.
   * @return formOfPayment
  **/
  @ApiModelProperty(value = "The purchased form of payment.")
  public String getFormOfPayment() {
    return formOfPayment;
  }

  public void setFormOfPayment(String formOfPayment) {
    this.formOfPayment = formOfPayment;
  }

  public Model114 programmedFlights(ProgrammedFlights programmedFlights) {
    this.programmedFlights = programmedFlights;
    return this;
  }

   /**
   * Get programmedFlights
   * @return programmedFlights
  **/
  @ApiModelProperty(value = "")
  public ProgrammedFlights getProgrammedFlights() {
    return programmedFlights;
  }

  public void setProgrammedFlights(ProgrammedFlights programmedFlights) {
    this.programmedFlights = programmedFlights;
  }

  public Model114 replacementFlights(ReplacementFlights replacementFlights) {
    this.replacementFlights = replacementFlights;
    return this;
  }

   /**
   * Get replacementFlights
   * @return replacementFlights
  **/
  @ApiModelProperty(value = "")
  public ReplacementFlights getReplacementFlights() {
    return replacementFlights;
  }

  public void setReplacementFlights(ReplacementFlights replacementFlights) {
    this.replacementFlights = replacementFlights;
  }

  public Model114 timeLost(String timeLost) {
    this.timeLost = timeLost;
    return this;
  }

   /**
   * Time lost perceived by the affected person. Format ISO 8601 (PTxxHxxMxxS). Example for a duration of 15h32 it would be PT15H32M00S.
   * @return timeLost
  **/
  @ApiModelProperty(value = "Time lost perceived by the affected person. Format ISO 8601 (PTxxHxxMxxS). Example for a duration of 15h32 it would be PT15H32M00S.")
  public String getTimeLost() {
    return timeLost;
  }

  public void setTimeLost(String timeLost) {
    this.timeLost = timeLost;
  }

  public Model114 agencyInformation(Model113 agencyInformation) {
    this.agencyInformation = agencyInformation;
    return this;
  }

   /**
   * Get agencyInformation
   * @return agencyInformation
  **/
  @ApiModelProperty(value = "")
  public Model113 getAgencyInformation() {
    return agencyInformation;
  }

  public void setAgencyInformation(Model113 agencyInformation) {
    this.agencyInformation = agencyInformation;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Model114 model114 = (Model114) o;
    return Objects.equals(this.isAirlineCompensation, model114.isAirlineCompensation) &&
        Objects.equals(this.formOfPayment, model114.formOfPayment) &&
        Objects.equals(this.programmedFlights, model114.programmedFlights) &&
        Objects.equals(this.replacementFlights, model114.replacementFlights) &&
        Objects.equals(this.timeLost, model114.timeLost) &&
        Objects.equals(this.agencyInformation, model114.agencyInformation);
  }

  @Override
  public int hashCode() {
    return Objects.hash(isAirlineCompensation, formOfPayment, programmedFlights, replacementFlights, timeLost, agencyInformation);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Model114 {\n");
    
    sb.append("    isAirlineCompensation: ").append(toIndentedString(isAirlineCompensation)).append("\n");
    sb.append("    formOfPayment: ").append(toIndentedString(formOfPayment)).append("\n");
    sb.append("    programmedFlights: ").append(toIndentedString(programmedFlights)).append("\n");
    sb.append("    replacementFlights: ").append(toIndentedString(replacementFlights)).append("\n");
    sb.append("    timeLost: ").append(toIndentedString(timeLost)).append("\n");
    sb.append("    agencyInformation: ").append(toIndentedString(agencyInformation)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

