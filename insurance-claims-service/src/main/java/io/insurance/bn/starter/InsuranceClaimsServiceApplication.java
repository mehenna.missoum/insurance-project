package io.insurance.bn.starter;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 *
 * @author mehenna
 * @version 0.0.1
 *
 */

@SpringBootApplication
@EnableDiscoveryClient
public class InsuranceClaimsServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(InsuranceClaimsServiceApplication.class, args);
    }

}

