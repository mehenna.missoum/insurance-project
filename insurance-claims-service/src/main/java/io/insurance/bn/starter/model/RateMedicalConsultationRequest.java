/*
 * API documentation
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: 1.0.0-20181108155220-20181108155343
 * Contact: api.move@axa-assistance.com
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */


package io.insurance.bn.starter.model;

import com.google.gson.annotations.SerializedName;
import io.swagger.annotations.ApiModelProperty;

import java.math.BigDecimal;
import java.util.Objects;

/**
 * RateMedicalConsultationRequest
 */
@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaClientCodegen", date = "2018-12-19T23:23:06.995+01:00")
public class RateMedicalConsultationRequest {
  @SerializedName("rating")
  private BigDecimal rating = null;

  @SerializedName("comments")
  private String comments = null;

  public RateMedicalConsultationRequest rating(BigDecimal rating) {
    this.rating = rating;
    return this;
  }

   /**
   * Rating of the medical consultation. Expected value between 0 and 5.
   * minimum: 0
   * maximum: 5
   * @return rating
  **/
  @ApiModelProperty(example = "3.0", required = true, value = "Rating of the medical consultation. Expected value between 0 and 5.")
  public BigDecimal getRating() {
    return rating;
  }

  public void setRating(BigDecimal rating) {
    this.rating = rating;
  }

  public RateMedicalConsultationRequest comments(String comments) {
    this.comments = comments;
    return this;
  }

   /**
   * Comments regarding the rating
   * @return comments
  **/
  @ApiModelProperty(example = "Good", value = "Comments regarding the rating")
  public String getComments() {
    return comments;
  }

  public void setComments(String comments) {
    this.comments = comments;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    RateMedicalConsultationRequest rateMedicalConsultationRequest = (RateMedicalConsultationRequest) o;
    return Objects.equals(this.rating, rateMedicalConsultationRequest.rating) &&
        Objects.equals(this.comments, rateMedicalConsultationRequest.comments);
  }

  @Override
  public int hashCode() {
    return Objects.hash(rating, comments);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class RateMedicalConsultationRequest {\n");
    
    sb.append("    rating: ").append(toIndentedString(rating)).append("\n");
    sb.append("    comments: ").append(toIndentedString(comments)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

