/*
 * API documentation
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: 1.0.0-20181108155220-20181108155343
 * Contact: api.move@axa-assistance.com
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */


package io.insurance.bn.starter.model;

import com.google.gson.annotations.SerializedName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.threeten.bp.LocalDate;

import java.util.Objects;

/**
 * If the customer has already declared a claim via another medium(i.e.phone), a case already exist in the IS. These information should have been provided to the customer while he was creating the case in this other medium.
 */
@ApiModel(description = "If the customer has already declared a claim via another medium(i.e.phone), a case already exist in the IS. These information should have been provided to the customer while he was creating the case in this other medium.")
@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaClientCodegen", date = "2018-12-19T23:23:06.995+01:00")
public class Reference {
  @SerializedName("number")
  private String number = null;

  @SerializedName("date")
  private LocalDate date = null;

  public Reference number(String number) {
    this.number = number;
    return this;
  }

   /**
   * Reference number that should have be provided by an agent to the affected person when he reported the claim in another medium.
   * @return number
  **/
  @ApiModelProperty(value = "Reference number that should have be provided by an agent to the affected person when he reported the claim in another medium.")
  public String getNumber() {
    return number;
  }

  public void setNumber(String number) {
    this.number = number;
  }

  public Reference date(LocalDate date) {
    this.date = date;
    return this;
  }

   /**
   * Date and time of the creation of the reference. UTC datetime, RFC3339 format (YYYY-MM-DDTHH:mm:ssZ).
   * @return date
  **/
  @ApiModelProperty(value = "Date and time of the creation of the reference. UTC datetime, RFC3339 format (YYYY-MM-DDTHH:mm:ssZ).")
  public LocalDate getDate() {
    return date;
  }

  public void setDate(LocalDate date) {
    this.date = date;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Reference reference = (Reference) o;
    return Objects.equals(this.number, reference.number) &&
        Objects.equals(this.date, reference.date);
  }

  @Override
  public int hashCode() {
    return Objects.hash(number, date);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Reference {\n");
    
    sb.append("    number: ").append(toIndentedString(number)).append("\n");
    sb.append("    date: ").append(toIndentedString(date)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

