/*
 * API documentation
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: 1.0.0-20181108155220-20181108155343
 * Contact: api.move@axa-assistance.com
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */


package io.insurance.bn.starter.model;

import com.google.gson.annotations.SerializedName;
import io.swagger.annotations.ApiModelProperty;

import java.util.Objects;

/**
 * HealthRisks
 */
@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaClientCodegen", date = "2018-12-19T23:23:06.995+01:00")
public class HealthRisks {
  @SerializedName("label")
  private String label = null;

  @SerializedName("bird")
  private String bird = null;

  @SerializedName("traveler")
  private String traveler = null;

  @SerializedName("malaria")
  private String malaria = null;

  @SerializedName("special")
  private String special = null;

  @SerializedName("food")
  private String food = null;

  @SerializedName("common")
  private String common = null;

  public HealthRisks label(String label) {
    this.label = label;
    return this;
  }

   /**
   * Health Risks label. Ex: \&quot;Health Risks\&quot;
   * @return label
  **/
  @ApiModelProperty(value = "Health Risks label. Ex: \"Health Risks\"")
  public String getLabel() {
    return label;
  }

  public void setLabel(String label) {
    this.label = label;
  }

  public HealthRisks bird(String bird) {
    this.bird = bird;
    return this;
  }

   /**
   * Bird risks related description. Ex: \&quot;There is a low risk of bird flu/avian...\&quot;
   * @return bird
  **/
  @ApiModelProperty(value = "Bird risks related description. Ex: \"There is a low risk of bird flu/avian...\"")
  public String getBird() {
    return bird;
  }

  public void setBird(String bird) {
    this.bird = bird;
  }

  public HealthRisks traveler(String traveler) {
    this.traveler = traveler;
    return this;
  }

   /**
   * Traveler risks related description. Ex: \&quot;The risk of travelers&#39;s diarrhea is high in...\&quot;
   * @return traveler
  **/
  @ApiModelProperty(value = "Traveler risks related description. Ex: \"The risk of travelers's diarrhea is high in...\"")
  public String getTraveler() {
    return traveler;
  }

  public void setTraveler(String traveler) {
    this.traveler = traveler;
  }

  public HealthRisks malaria(String malaria) {
    this.malaria = malaria;
    return this;
  }

   /**
   * Malaria risks related description. Ex: \&quot;According to WHO, Colombia is a malaria affected country...\&quot;
   * @return malaria
  **/
  @ApiModelProperty(value = "Malaria risks related description. Ex: \"According to WHO, Colombia is a malaria affected country...\"")
  public String getMalaria() {
    return malaria;
  }

  public void setMalaria(String malaria) {
    this.malaria = malaria;
  }

  public HealthRisks special(String special) {
    this.special = special;
    return this;
  }

   /**
   * Special risks related description. Ex: \&quot;Human cases of Swine Flu have been reported in...\&quot;
   * @return special
  **/
  @ApiModelProperty(value = "Special risks related description. Ex: \"Human cases of Swine Flu have been reported in...\"")
  public String getSpecial() {
    return special;
  }

  public void setSpecial(String special) {
    this.special = special;
  }

  public HealthRisks food(String food) {
    this.food = food;
    return this;
  }

   /**
   * Food risks related description. Ex: \&quot;Tap water in Colombia is not safe for drinking. Therefore,...\&quot;
   * @return food
  **/
  @ApiModelProperty(value = "Food risks related description. Ex: \"Tap water in Colombia is not safe for drinking. Therefore,...\"")
  public String getFood() {
    return food;
  }

  public void setFood(String food) {
    this.food = food;
  }

  public HealthRisks common(String common) {
    this.common = common;
    return this;
  }

   /**
   * Common risks related description. Ex: \&quot;Bartonellosis (Oroya fever): Moderate to High Risk...\&quot;
   * @return common
  **/
  @ApiModelProperty(value = "Common risks related description. Ex: \"Bartonellosis (Oroya fever): Moderate to High Risk...\"")
  public String getCommon() {
    return common;
  }

  public void setCommon(String common) {
    this.common = common;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    HealthRisks healthRisks = (HealthRisks) o;
    return Objects.equals(this.label, healthRisks.label) &&
        Objects.equals(this.bird, healthRisks.bird) &&
        Objects.equals(this.traveler, healthRisks.traveler) &&
        Objects.equals(this.malaria, healthRisks.malaria) &&
        Objects.equals(this.special, healthRisks.special) &&
        Objects.equals(this.food, healthRisks.food) &&
        Objects.equals(this.common, healthRisks.common);
  }

  @Override
  public int hashCode() {
    return Objects.hash(label, bird, traveler, malaria, special, food, common);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class HealthRisks {\n");
    
    sb.append("    label: ").append(toIndentedString(label)).append("\n");
    sb.append("    bird: ").append(toIndentedString(bird)).append("\n");
    sb.append("    traveler: ").append(toIndentedString(traveler)).append("\n");
    sb.append("    malaria: ").append(toIndentedString(malaria)).append("\n");
    sb.append("    special: ").append(toIndentedString(special)).append("\n");
    sb.append("    food: ").append(toIndentedString(food)).append("\n");
    sb.append("    common: ").append(toIndentedString(common)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

