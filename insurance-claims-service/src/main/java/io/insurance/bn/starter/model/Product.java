/*
 * API documentation
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: 1.0.0-20181108155220-20181108155343
 * Contact: api.move@axa-assistance.com
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */


package io.insurance.bn.starter.model;

import com.google.gson.annotations.SerializedName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.util.Objects;

/**
 * Context of the request.
 */
@ApiModel(description = "Context of the request.")
@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaClientCodegen", date = "2018-12-19T23:23:06.995+01:00")
public class Product {
  @SerializedName("country")
  private String country = null;

  @SerializedName("currency")
  private String currency = null;

  public Product country(String country) {
    this.country = country;
    return this;
  }

   /**
   * Customer country, ISO-3166-1 alpha-2 format (2 letter codes).
   * @return country
  **/
  @ApiModelProperty(example = "FR", value = "Customer country, ISO-3166-1 alpha-2 format (2 letter codes).")
  public String getCountry() {
    return country;
  }

  public void setCountry(String country) {
    this.country = country;
  }

  public Product currency(String currency) {
    this.currency = currency;
    return this;
  }

   /**
   * Customer currency, ISO 4217 format (3 letter code).
   * @return currency
  **/
  @ApiModelProperty(example = "EUR", required = true, value = "Customer currency, ISO 4217 format (3 letter code).")
  public String getCurrency() {
    return currency;
  }

  public void setCurrency(String currency) {
    this.currency = currency;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Product product = (Product) o;
    return Objects.equals(this.country, product.country) &&
        Objects.equals(this.currency, product.currency);
  }

  @Override
  public int hashCode() {
    return Objects.hash(country, currency);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Product {\n");
    
    sb.append("    country: ").append(toIndentedString(country)).append("\n");
    sb.append("    currency: ").append(toIndentedString(currency)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

