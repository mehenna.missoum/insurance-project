/*
 * API documentation
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: 1.0.0-20181108155220-20181108155343
 * Contact: api.move@axa-assistance.com
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */


package io.insurance.bn.starter.model;

import com.google.gson.annotations.SerializedName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.util.Objects;

/**
 * Technician in charge of providing the service.
 */
@ApiModel(description = "Technician in charge of providing the service.")
@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaClientCodegen", date = "2018-12-19T23:23:06.995+01:00")
public class Technician {
  @SerializedName("location")
  private Model23 location = null;

  @SerializedName("truck_registration_number")
  private String truckRegistrationNumber = null;

  public Technician location(Model23 location) {
    this.location = location;
    return this;
  }

   /**
   * Get location
   * @return location
  **/
  @ApiModelProperty(value = "")
  public Model23 getLocation() {
    return location;
  }

  public void setLocation(Model23 location) {
    this.location = location;
  }

  public Technician truckRegistrationNumber(String truckRegistrationNumber) {
    this.truckRegistrationNumber = truckRegistrationNumber;
    return this;
  }

   /**
   * Registration number of technician&#39;s truck.
   * @return truckRegistrationNumber
  **/
  @ApiModelProperty(example = "123TR45", value = "Registration number of technician's truck.")
  public String getTruckRegistrationNumber() {
    return truckRegistrationNumber;
  }

  public void setTruckRegistrationNumber(String truckRegistrationNumber) {
    this.truckRegistrationNumber = truckRegistrationNumber;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Technician technician = (Technician) o;
    return Objects.equals(this.location, technician.location) &&
        Objects.equals(this.truckRegistrationNumber, technician.truckRegistrationNumber);
  }

  @Override
  public int hashCode() {
    return Objects.hash(location, truckRegistrationNumber);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Technician {\n");
    
    sb.append("    location: ").append(toIndentedString(location)).append("\n");
    sb.append("    truckRegistrationNumber: ").append(toIndentedString(truckRegistrationNumber)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

