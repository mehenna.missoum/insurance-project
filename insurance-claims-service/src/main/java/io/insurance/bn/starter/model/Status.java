/*
 * API documentation
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: 1.0.0-20181108155220-20181108155343
 * Contact: api.move@axa-assistance.com
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */


package io.insurance.bn.starter.model;

import com.google.gson.annotations.SerializedName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.util.Objects;

/**
 * Policy&#39;s status details.
 */
@ApiModel(description = "Policy's status details.")
@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaClientCodegen", date = "2018-12-19T23:23:06.995+01:00")
public class Status {
  @SerializedName("value")
  private String value = null;

  @SerializedName("reason")
  private String reason = null;

  public Status value(String value) {
    this.value = value;
    return this;
  }

   /**
   * Status value. Expected values (CONFIRMED, DRAFT)
   * @return value
  **/
  @ApiModelProperty(example = "DRAFT", required = true, value = "Status value. Expected values (CONFIRMED, DRAFT)")
  public String getValue() {
    return value;
  }

  public void setValue(String value) {
    this.value = value;
  }

  public Status reason(String reason) {
    this.reason = reason;
    return this;
  }

   /**
   * Status reason
   * @return reason
  **/
  @ApiModelProperty(example = "This is a reason.", value = "Status reason")
  public String getReason() {
    return reason;
  }

  public void setReason(String reason) {
    this.reason = reason;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Status status = (Status) o;
    return Objects.equals(this.value, status.value) &&
        Objects.equals(this.reason, status.reason);
  }

  @Override
  public int hashCode() {
    return Objects.hash(value, reason);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Status {\n");
    
    sb.append("    value: ").append(toIndentedString(value)).append("\n");
    sb.append("    reason: ").append(toIndentedString(reason)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

