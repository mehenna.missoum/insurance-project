package io.insurance.bn.starter.service;

import io.insurance.bn.starter.model.Model234;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;
import java.util.Map;

/**
 *
 * @author mehenna
 * @version 0.0.1
 *
 */

@Service
public class ClaimService {
    /**
     * Http Client
     */
    @Autowired
    private RestTemplate template;

    /**
     * endPointClaimTypes
     */
    @Value( "${insurance.vexp.types}" )
    private String endPointClaimTypes;

    /**
     * Param ndPointClaimAttachements
     */
    @Value( "${baggage.claims.attachments}" )
    private String endPointClaimAttachements;

    /**
     *
     * @param contractId
     * @return
     */
    public  String getClaimTypes (String contractId ) {
        final ResponseEntity<String> responseEntity = this.template.getForEntity(endPointClaimTypes+contractId, String.class);
        return responseEntity.getBody().toString();
    }

    /**
     *
     * @param model
     * @return
     */
    public  String addAttachementToClaim (Model234 model, String claimId) {
        ResponseEntity<String> postResponse = this.template.postForEntity(endPointClaimAttachements, model, String.class, claimId);
        return  postResponse.getBody().toString();
    }

    /**
     *
     * @param attachementData
     * @param claimId
     * @return
     */
    public  String addAttachementToClaim (Map<String, String> attachementData, String claimId) {
        ResponseEntity<String> postResponse = this.template.postForEntity(endPointClaimAttachements, attachementData, String.class, claimId);
        return  postResponse.getBody().toString();
    }

    /**
     *
     * @return
     */
    private HttpHeaders  getHeader() {
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(new MediaType[] { MediaType.APPLICATION_JSON }));
        headers.setContentType(MediaType.APPLICATION_JSON);
        return headers;
    }
}
