package io.insurance.bn.starter.controller;

import io.insurance.bn.starter.model.Model234;
import io.insurance.bn.starter.service.ClaimService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.*;

import javax.ws.rs.QueryParam;
import java.util.Map;
import java.util.stream.Collectors;

/**
 *
 * @author mehenna
 * @version 0.0.1
 *
 */

@RefreshScope
@RestController
@Api(value="Claims", description = "Data service operations on claims", tags=("insurance-claims"))
public class ClaimsController {

    private final Logger logger = LoggerFactory.getLogger(ClaimService.class);
    /**
     * Claims service
     */
    private ClaimService claimService;

    /**
     * Autowiring the service
     * @param claimService
     */
    public ClaimsController(ClaimService claimService) {
        this.claimService = claimService;
    }

    /**
     *
     * @param contractId
     * @return
     */
    @GetMapping("/claims")
    @ApiOperation(value="Get All claim types", notes="Gets claim typs exposed by AXA", nickname="getClaimsTypes")
    public String getClaimsTypes(@QueryParam("contract_id") String contractId){
        return claimService.getClaimTypes(contractId);
    }

    /**
     *
     * @param model
     * @param claimId
     * @return
     */
    @PostMapping("/{claim_id}/attachment")
    @ApiOperation(value="Add attached document to claim", notes="Attach document to created claim ", nickname="addAttachementToClaim")
    public  String addAttachementToClaim (@RequestBody Model234 model, @PathVariable("claim_id") String claimId) {
        logger.debug("Incomming data  : 1-Model " + model.toString() + " 2. claim_id " + claimId );
         return this.claimService.addAttachementToClaim(model, claimId);
    }

    /**
     *
     * @param data
     * @param claimId
     * @return
     */
    @PostMapping("/{claim_id}/attachmentsm")
    @ApiOperation(value="Add attached document to claim", notes="Attach document to created claim ", nickname="addAttachementToClaim")
    public  String addAttachementToClaimss (@RequestBody Map<String, String> data, @PathVariable("claim_id") String claimId) {
        String input = data.entrySet().stream()
                            .map(entry -> entry.getKey() + "--" + entry.getValue())
                            .collect(Collectors.joining(",  "));
        logger.debug("Incomming data  : 1-Model " + input + " 2. claim_id " + claimId );
        return  this.claimService.addAttachementToClaim(data, claimId);
    }

}
