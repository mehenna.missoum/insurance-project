/*
 * API documentation
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: 1.0.0-20181108155220-20181108155343
 * Contact: api.move@axa-assistance.com
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */


package io.insurance.bn.starter.model;

import com.google.gson.annotations.SerializedName;
import io.swagger.annotations.ApiModelProperty;

import java.util.Objects;

/**
 * Phone
 */
@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaClientCodegen", date = "2018-12-19T23:23:06.995+01:00")
public class Phone {
  @SerializedName("phone_type")
  private String phoneType = null;

  @SerializedName("international_prefix")
  private String internationalPrefix = null;

  @SerializedName("number")
  private String number = null;

  public Phone phoneType(String phoneType) {
    this.phoneType = phoneType;
    return this;
  }

   /**
   * Type of number. Expected values (MOBILE, LANDLINE)
   * @return phoneType
  **/
  @ApiModelProperty(example = "MOBILE", value = "Type of number. Expected values (MOBILE, LANDLINE)")
  public String getPhoneType() {
    return phoneType;
  }

  public void setPhoneType(String phoneType) {
    this.phoneType = phoneType;
  }

  public Phone internationalPrefix(String internationalPrefix) {
    this.internationalPrefix = internationalPrefix;
    return this;
  }

   /**
   * Prefix to be added to be called from abroad
   * @return internationalPrefix
  **/
  @ApiModelProperty(example = "+33", value = "Prefix to be added to be called from abroad")
  public String getInternationalPrefix() {
    return internationalPrefix;
  }

  public void setInternationalPrefix(String internationalPrefix) {
    this.internationalPrefix = internationalPrefix;
  }

  public Phone number(String number) {
    this.number = number;
    return this;
  }

   /**
   * Phone number. Can be local or international
   * @return number
  **/
  @ApiModelProperty(example = "0123456789", required = true, value = "Phone number. Can be local or international")
  public String getNumber() {
    return number;
  }

  public void setNumber(String number) {
    this.number = number;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Phone phone = (Phone) o;
    return Objects.equals(this.phoneType, phone.phoneType) &&
        Objects.equals(this.internationalPrefix, phone.internationalPrefix) &&
        Objects.equals(this.number, phone.number);
  }

  @Override
  public int hashCode() {
    return Objects.hash(phoneType, internationalPrefix, number);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Phone {\n");
    
    sb.append("    phoneType: ").append(toIndentedString(phoneType)).append("\n");
    sb.append("    internationalPrefix: ").append(toIndentedString(internationalPrefix)).append("\n");
    sb.append("    number: ").append(toIndentedString(number)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

