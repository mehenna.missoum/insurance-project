/*
 * API documentation
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: 1.0.0-20181108155220-20181108155343
 * Contact: api.move@axa-assistance.com
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */


package io.insurance.bn.starter.model;

import com.google.gson.annotations.SerializedName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.util.Objects;

/**
 * Bank account for the reimbursement
 */
@ApiModel(description = "Bank account for the reimbursement")
@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaClientCodegen", date = "2018-12-19T23:23:06.995+01:00")
public class BeneficiaryAccount {
  @SerializedName("bank_account_number")
  private String bankAccountNumber = null;

  @SerializedName("wire_codes")
  private WireCodes wireCodes = null;

  @SerializedName("is_current_account")
  private Boolean isCurrentAccount = null;

  @SerializedName("holder")
  private Holder holder = null;

  @SerializedName("bank")
  private Bank bank = null;

  public BeneficiaryAccount bankAccountNumber(String bankAccountNumber) {
    this.bankAccountNumber = bankAccountNumber;
    return this;
  }

   /**
   * Bank account number
   * @return bankAccountNumber
  **/
  @ApiModelProperty(example = "1245678", value = "Bank account number")
  public String getBankAccountNumber() {
    return bankAccountNumber;
  }

  public void setBankAccountNumber(String bankAccountNumber) {
    this.bankAccountNumber = bankAccountNumber;
  }

  public BeneficiaryAccount wireCodes(WireCodes wireCodes) {
    this.wireCodes = wireCodes;
    return this;
  }

   /**
   * Get wireCodes
   * @return wireCodes
  **/
  @ApiModelProperty(value = "")
  public WireCodes getWireCodes() {
    return wireCodes;
  }

  public void setWireCodes(WireCodes wireCodes) {
    this.wireCodes = wireCodes;
  }

  public BeneficiaryAccount isCurrentAccount(Boolean isCurrentAccount) {
    this.isCurrentAccount = isCurrentAccount;
    return this;
  }

   /**
   * Is it a current account?
   * @return isCurrentAccount
  **/
  @ApiModelProperty(example = "true", value = "Is it a current account?")
  public Boolean isIsCurrentAccount() {
    return isCurrentAccount;
  }

  public void setIsCurrentAccount(Boolean isCurrentAccount) {
    this.isCurrentAccount = isCurrentAccount;
  }

  public BeneficiaryAccount holder(Holder holder) {
    this.holder = holder;
    return this;
  }

   /**
   * Get holder
   * @return holder
  **/
  @ApiModelProperty(value = "")
  public Holder getHolder() {
    return holder;
  }

  public void setHolder(Holder holder) {
    this.holder = holder;
  }

  public BeneficiaryAccount bank(Bank bank) {
    this.bank = bank;
    return this;
  }

   /**
   * Get bank
   * @return bank
  **/
  @ApiModelProperty(value = "")
  public Bank getBank() {
    return bank;
  }

  public void setBank(Bank bank) {
    this.bank = bank;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    BeneficiaryAccount beneficiaryAccount = (BeneficiaryAccount) o;
    return Objects.equals(this.bankAccountNumber, beneficiaryAccount.bankAccountNumber) &&
        Objects.equals(this.wireCodes, beneficiaryAccount.wireCodes) &&
        Objects.equals(this.isCurrentAccount, beneficiaryAccount.isCurrentAccount) &&
        Objects.equals(this.holder, beneficiaryAccount.holder) &&
        Objects.equals(this.bank, beneficiaryAccount.bank);
  }

  @Override
  public int hashCode() {
    return Objects.hash(bankAccountNumber, wireCodes, isCurrentAccount, holder, bank);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class BeneficiaryAccount {\n");
    
    sb.append("    bankAccountNumber: ").append(toIndentedString(bankAccountNumber)).append("\n");
    sb.append("    wireCodes: ").append(toIndentedString(wireCodes)).append("\n");
    sb.append("    isCurrentAccount: ").append(toIndentedString(isCurrentAccount)).append("\n");
    sb.append("    holder: ").append(toIndentedString(holder)).append("\n");
    sb.append("    bank: ").append(toIndentedString(bank)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

