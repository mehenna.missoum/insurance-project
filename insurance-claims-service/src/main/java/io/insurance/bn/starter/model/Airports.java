/*
 * API documentation
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: 1.0.0-20181108155220-20181108155343
 * Contact: api.move@axa-assistance.com
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */


package io.insurance.bn.starter.model;

import com.google.gson.annotations.SerializedName;
import io.swagger.annotations.ApiModelProperty;

import java.util.Objects;

/**
 * Airports
 */
@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaClientCodegen", date = "2018-12-19T23:23:06.995+01:00")
public class Airports {
  @SerializedName("label")
  private String label = null;

  @SerializedName("desc")
  private String desc = null;

  public Airports label(String label) {
    this.label = label;
    return this;
  }

   /**
   * Airports label. Ex: \&quot;Airports\&quot;
   * @return label
  **/
  @ApiModelProperty(value = "Airports label. Ex: \"Airports\"")
  public String getLabel() {
    return label;
  }

  public void setLabel(String label) {
    this.label = label;
  }

  public Airports desc(String desc) {
    this.desc = desc;
    return this;
  }

   /**
   * Airports description. Ex: \&quot;Major international airports of Thailand...\&quot;
   * @return desc
  **/
  @ApiModelProperty(value = "Airports description. Ex: \"Major international airports of Thailand...\"")
  public String getDesc() {
    return desc;
  }

  public void setDesc(String desc) {
    this.desc = desc;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Airports airports = (Airports) o;
    return Objects.equals(this.label, airports.label) &&
        Objects.equals(this.desc, airports.desc);
  }

  @Override
  public int hashCode() {
    return Objects.hash(label, desc);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Airports {\n");
    
    sb.append("    label: ").append(toIndentedString(label)).append("\n");
    sb.append("    desc: ").append(toIndentedString(desc)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

