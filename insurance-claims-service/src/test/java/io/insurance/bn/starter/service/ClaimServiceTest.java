package io.insurance.bn.starter.service;

import org.junit.Test;
import static org.assertj.core.api.Assertions.assertThat;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 *
 * @author mehenna
 * @version 0.0.1-SNAPSHOT
 *
 */


@SpringBootTest
@RunWith(SpringRunner.class)
public class ClaimServiceTest {

    @InjectMocks
    ClaimService  claimService;


    /**
     *
     */
    @Test
    public  void init(){
        assertThat(claimService.getClaimTypes("12345")).isNotEmpty();
    }

    /**
     *
     */
    @Test
    public void addAttachementToClaim(){
       // assertThat(claimService.addAttachementToClaim(null)).isNotEmpty();
    }
}